/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.model.response;

import io.swagger.annotations.ApiModel;

import java.util.List;
import java.util.Map;

@ApiModel("表数据内容")
public class SchemaTableDataResponse {

    private String schemaName;
    private String tableName;
    private List<String> columns;
    private List<Map<String, Object>> rows;

    public static SchemaTableDataResponse builder() {
        SchemaTableDataResponse schemaTableDataResponse = new SchemaTableDataResponse();
        return schemaTableDataResponse;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public SchemaTableDataResponse schemaName(String schemaName) {
        this.schemaName = schemaName;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public SchemaTableDataResponse tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public List<String> getColumns() {
        return columns;
    }

    public SchemaTableDataResponse columns(List<String> columns) {
        this.columns = columns;
        return this;
    }

    public List<Map<String, Object>> getRows() {
        return rows;
    }

    public SchemaTableDataResponse rows(List<Map<String, Object>> rows) {
        this.rows = rows;
        return this;
    }


}
