/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.service.dbswitch.response;


public enum ResultCode {

  SUCCESS(0, "操作成功"),
  ERROR_INTERNAL_ERROR(1, "内部错误"),
  ERROR_INVALID_ARGUMENT(2, "无效参数"),
  ERROR_RESOURCE_NOT_EXISTS(3, "资源不存在"),
  ERROR_RESOURCE_ALREADY_EXISTS(4, "资源已存在"),
  ERROR_RESOURCE_NOT_DEPLOY(5, "资源未发布"),
  ERROR_RESOURCE_HAS_DEPLOY(6, "资源已发布"),
  ERROR_USER_NOT_EXISTS(7, "用户不存在"),
  ERROR_USER_PASSWORD_WRONG(8, "密码错误"),
  ERROR_INVALID_JDBC_URL(9, "JDBC连接的URL格式不正确"),
  ERROR_CANNOT_CONNECT_REMOTE(10, "远程地址不可达"),
  ERROR_INVALID_ASSIGNMENT_CONFIG(11, "无效的任务参数配置"),

  ERROR_ACCESS_FORBIDDEN(403, "无效的登陆凭证"),
  ERROR_TOKEN_EXPIRED(404, "登录凭证已失效"),
  ;

  private int code;
  private String message;

  ResultCode(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
