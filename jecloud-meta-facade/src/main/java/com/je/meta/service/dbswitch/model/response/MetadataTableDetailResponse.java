/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.model.response;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel("表元数据详情")
public class MetadataTableDetailResponse {

    private String tableName;
    private String schemaName;
    private String remarks;
    private String type;
    private String createSql;
    private List<String> primaryKeys;
    private List<MetadataColumnDetailResponse> columns;


    public String getTableName() {
        return tableName;
    }

    public MetadataTableDetailResponse tableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public MetadataTableDetailResponse schemaName(String schemaName) {
        this.schemaName = schemaName;
        return this;
    }

    public String getRemarks() {
        return remarks;
    }

    public MetadataTableDetailResponse remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public String getType() {
        return type;
    }

    public MetadataTableDetailResponse type(String type) {
        this.type = type;
        return this;
    }

    public String getCreateSql() {
        return createSql;
    }

    public MetadataTableDetailResponse createSql(String createSql) {
        this.createSql = createSql;
        return this;
    }

    public List<String> getPrimaryKeys() {
        return primaryKeys;
    }

    public MetadataTableDetailResponse primaryKeys(List<String> primaryKeys) {
        this.primaryKeys = primaryKeys;
        return this;
    }

    public List<MetadataColumnDetailResponse> getColumns() {
        return columns;
    }

    public MetadataTableDetailResponse columns(List<MetadataColumnDetailResponse> columns) {
        this.columns = columns;
        return this;
    }

    public static MetadataTableDetailResponse builder() {
        return new MetadataTableDetailResponse();
    }

}
