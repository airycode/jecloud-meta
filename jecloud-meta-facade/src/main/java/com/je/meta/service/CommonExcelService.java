/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CommonExcelService {

    /**
     * 核心表 - 后续补充
     */
    List<String> CORE_TABLE = Lists.newArrayList("JE_CORE_DICTIONARY");


    default Map<String, String> getRequestParams(HttpServletRequest request) {
        Enumeration ener = request.getParameterNames();
        Map<String, String> params = new HashMap<String, String>();
        while (ener.hasMoreElements()) {
            String key = ener.nextElement() + "";
            params.put(key, request.getParameter(key));
        }
        return params;
    }

    JSONObject impData(DynaBean groupTemBean, String fileKey, HttpServletRequest request);

    JSONObject impData(String code, String fileKey, HttpServletRequest request);

    /**
     * 导入数据
     *
     * @param code       编码
     * @param filePath   文件唯一标识
     * @param groupTemId Excel数据导入临时组ID
     * @param request    TODO未处理
     * @return net.sf.json.JSONObject
     */
    JSONObject impData(String code, String filePath, String groupTemId, HttpServletRequest request);

    /**
     * 窗口导入数据
     * @param temIds
     * @param groupCode
     * @param temGroupId
     * @param request
     * @return
     */
    Map<String,Object> impPreviewData(String temIds,String groupCode, String temGroupId, HttpServletRequest request);

    void doUpdate(DynaBean dynaBean, HttpServletRequest request);
}
