/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.service.dbswitch.excption;

import com.je.meta.service.dbswitch.response.ResultCode;

public class DbswitchException extends RuntimeException {

  private ResultCode code;
  private String message;

  public DbswitchException(ResultCode code, String message) {
    this.code = code;
    this.message = message;
  }

  public DbswitchException(String message, ResultCode code, String message1) {
    super(message);
    this.code = code;
    this.message = message1;
  }

  public DbswitchException(String message, Throwable cause, ResultCode code, String message1) {
    super(message, cause);
    this.code = code;
    this.message = message1;
  }

  public DbswitchException(Throwable cause, ResultCode code, String message) {
    super(cause);
    this.code = code;
    this.message = message;
  }

  public DbswitchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ResultCode code, String message1) {
    super(message, cause, enableSuppression, writableStackTrace);
    this.code = code;
    this.message = message1;
  }
}
