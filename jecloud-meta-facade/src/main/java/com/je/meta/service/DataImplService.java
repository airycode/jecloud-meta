/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;


import com.je.common.base.DynaBean;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.MathExtend;
import com.je.common.base.util.StringUtil;
import net.sf.json.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DataImplService {

    /**
     * 导入数据
     *
     * @param filePath 文件路径
     */
    List<DynaBean> implData(String filePath, JSONObject returnObj);

    default HSSFCell getCell(HSSFSheet sheet, Integer colnumber, Integer rownumber) {
        HSSFRow row = sheet.getRow(rownumber - 1);
        if (row == null) {
            return null;
        }
        HSSFCell cell = row.getCell(Short.parseShort((colnumber - 1) + ""));
        return cell;
    }

    default String getCellStringValue(HSSFCell cell) {
        String cellValue = "";
//        if (cell == null) {return "";}
//        switch (cell.getCellType()) {
//            // 字符串类型
//            case HSSFCell.CELL_TYPE_STRING:
//                cellValue = cell.getStringCellValue();
//                if ("".equals(cellValue.trim()) || cellValue.trim().length() <= 0) {
//                    cellValue = " ";
//                }
//                break;
//            // 数值类型
//            case HSSFCell.CELL_TYPE_NUMERIC:
//                if (HSSFDateUtil.isCellDateFormatted(cell)) {
//                    Date d = cell.getDateCellValue();
//                    if (d != null) {
//                        cellValue = DateUtils.formatDate(d, DateUtils.DAFAULT_DATETIME_FORMAT);
//                    } else {
//                        cellValue = "";
//                    }
//                } else {
//                    cellValue = String.valueOf(MathExtend.divide(
//                            cell.getNumericCellValue(), 1, 2));
////				cellValue=cell.getNumericCellValue()+"";
//                    int index = cellValue.lastIndexOf(".0");
//                    if (index != -1 && index == (cellValue.length() - 2)) {
//                        cellValue = cellValue.substring(0, cellValue.length() - 2);
//                    }
//                }
//                break;
//            // 公式
//            case HSSFCell.CELL_TYPE_FORMULA:
//                cell.setCellType(HSSFCell.CELL_TYPE_STRING);
////			cellValue = String.valueOf(MathExtend.divide(
////					cell.getNumericCellValue(), 1, 2));
//                cellValue = cell.getStringCellValue();
//                break;
//            case HSSFCell.CELL_TYPE_BLANK:
//                cellValue = " ";
//                break;
//            case HSSFCell.CELL_TYPE_BOOLEAN:
//                break;
//            case HSSFCell.CELL_TYPE_ERROR:
//                break;
//            default:
//                break;
//        }
        return cellValue;
    }

    /**
     * 获取字典字段的code的name值
     *
     * @param ddInfos
     * @param column
     * @param codeValue
     * @return
     */
    default String getDdValue(Map<String, Map<String, String>> ddInfos, DynaBean column, String codeValue) {
        String xtype = column.getStr("DATAFIELD_XTYPE");
        //多选框
        Map<String, String> ddItems = null;
        String configInfo = column.getStr("DATAFIELD_CONFIGINFO");
        if (StringUtil.isNotEmpty(configInfo)) {
            String ddCode = configInfo.split(ArrayUtils.SPLIT)[0];
            ddItems = ddInfos.get(ddCode);
        }
        if (ddItems != null) {
            if ("cgroup".equals(xtype)) {
                if (StringUtil.isNotEmpty(codeValue)) {
                    String[] codeValues = codeValue.split(ArrayUtils.SPLIT);
                    for (Integer i = 0; i < codeValues.length; i++) {
                        if (StringUtil.isNotEmpty(codeValues[i]) && ddItems.get(codeValues[i]) != null) {
                            codeValues[i] = ddItems.get(codeValues[i]);
                        }
                    }
                    return StringUtil.buildSplitString(codeValues, ",");
                } else {
                    return codeValue;
                }
            } else {
                if (StringUtil.isNotEmpty(codeValue) && ddItems.get(codeValue) != null) {
                    return ddItems.get(codeValue);
                } else {
                    return codeValue;
                }
            }
        } else {
            return codeValue;
        }
    }

    default DynaBean doDynaBean(DynaBean dynaBean) {
        dynaBean.set("CLIENT_KHJZ", "客户1");
        return dynaBean;
    }

}
