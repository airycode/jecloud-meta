/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import org.apache.servicecomb.registry.DiscoveryManager;
import org.apache.servicecomb.registry.api.registry.Microservice;
import org.apache.servicecomb.registry.consumer.MicroserviceManager;
import org.apache.servicecomb.registry.consumer.MicroserviceVersion;
import org.apache.servicecomb.registry.consumer.MicroserviceVersions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ContractRpcService<T extends Map<String, Object>> {

    Logger LOGGER = LoggerFactory.getLogger(ContractRpcService.class.getName());

    List<T> getContract();

    default void buildMetadataMap(List<T> list) {
        try {
            Map<String, MicroserviceManager> appManagerMap = DiscoveryManager.INSTANCE.getAppManager().getApps();
            for (String key : appManagerMap.keySet()) {
                Map<String, MicroserviceVersions> versionsMap = appManagerMap.get(key).getVersionsByName();
                for (String vKey : versionsMap.keySet()) {
                    MicroserviceVersions microserviceVersions = versionsMap.get(vKey);
                    buildResultMap(microserviceVersions, list);
                }
            }
        } catch (Exception e) {
            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                LOGGER.error(stackTraceElement.toString());
            }
            LOGGER.error("获取契约信息异常！");
        }
    }


    default void buildResultMap(MicroserviceVersions microserviceVersions, List<T> list) {
        List<MicroserviceVersion> versions = new ArrayList<>();
        for (String k : microserviceVersions.getVersions().keySet()) {
            versions.add(microserviceVersions.getVersions().get(k));
        }
        for (MicroserviceVersion microserviceVersion : versions) {
            Map<String, Object> map = new HashMap<>();
            Microservice microservice = microserviceVersion.getMicroservice();
            map.put("SERVICE_ID", microservice.getServiceId());
            map.put("FRAMEWORK", microservice.getFramework().getName() + "-" + microservice.getFramework().getVersion());
            map.put("ENVIRONMENT", microservice.getEnvironment());
            map.put("APP_ID", microservice.getAppId());
            map.put("SERVICE_NAME", microservice.getServiceName());
            map.put("VERSION", microservice.getVersion());
            map.put("LEVEL", microservice.getLevel());
            map.put("SCHEMAS_LENGTH", microservice.getSchemas().size());
//            map.put("END_POINTS", StringUtils.join(microservice.getInstance().getEndpoints(), ","));
            list.add((T) map);
        }
    }

}
