/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.excel.converter;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;

import java.util.Map;

public class ExcelExportChooseConverter extends AbstractExcelExportConverter {

    @Override
    public Object converter(String value, DynaBean formColumn, Map<String, Map<String, String>> ddInfos) {
        String[] valueArray = value.split(",");
        String config = formColumn.getStr("RESOURCEFIELD_CONFIGINFO");
        String filedCode = formColumn.getStr("RESOURCECOLUMN_CODE");
        if (Strings.isNullOrEmpty(config)) {
            return value;
        }
        String[] configArray = config.split(",");
        String ddCode = configArray[0];
        Map<String, String> ddValues = ddInfos.get(ddCode);

        //只配置了一个字典 JE_DEMO_SEX
        StringBuffer sbValue = new StringBuffer();
        if (configArray.length == 1) {
            for (String formValue : valueArray) {
                sbValue.append(ddValues.get(formValue) + ",");
            }
        }

        //没有这样的配置，直接返回value JE_DEMO_SEX,FORM_XB_CODE~FORM_XB_NAME
        if (configArray.length == 2) {
            return value;
        }

        //完整的配置 JE_DEMO_SEX,FORM_XB_CODE~FORM_XB_NAME,code~text,S
        if (configArray.length == 3 || configArray.length == 4) {
            String[] filedCodeArray = configArray[1].split("~");
            String[] correspondenceArray = configArray[2].split("~");
            if (filedCodeArray.length != correspondenceArray.length) {
                return value;
            }

            int codeAddress = 0;
            for(int i =0;i<filedCodeArray.length;i++){
                codeAddress=i;
                if (filedCodeArray[i].equals(filedCode)) {
                    break;
                }
            }
            /*for (String code : filedCodeArray) {

            }*/
            if (correspondenceArray[codeAddress].equals("text")) {
                return value;
            }

            if (correspondenceArray[codeAddress].equals("code")) {
                for (String formValue : valueArray) {
                    sbValue.append(ddValues.get(formValue) + ",");
                }
            }

        }
        if (sbValue.length() > 0) {
            value = sbValue.substring(0, sbValue.length() - 1);
        }
        return value;
    }
}
