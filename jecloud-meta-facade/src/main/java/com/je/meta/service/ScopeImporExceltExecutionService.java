/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import com.je.meta.model.ExcelParamVo;
import com.je.meta.model.ExcelParameterVo;
import com.je.meta.model.ExcelReturnVo;
import java.util.HashMap;
import java.util.Map;

public interface ScopeImporExceltExecutionService {


    /**
     * 执行前
     * @param ddMap
     * @param excelParamVo
     * @return
     */
    ExcelReturnVo executeDataPre(Map<String, Object> ddMap, ExcelParameterVo excelParamVo);

    /**
     * 数据处理中
     */

    //JSONObject  executeData(DynaBean group,DynaBean sheet, String groupTemId, Map<String, Object> ddMap, Map<String, List<DynaBean>> excelData, List<DynaBean> sheets, HttpServletRequest request);

    /**
     * 执行后
     * @param ddMap
     * @param excelParamVo
     * @return
     */
    ExcelReturnVo executeDataAfter(Map<String, Object> ddMap, ExcelParameterVo excelParamVo);

    default ExcelReturnVo initExcelReturnVo(ExcelParamVo paramVo){
        ExcelReturnVo returnVo = new ExcelReturnVo(1, "成功");
        returnVo.setDoType(paramVo.getDoType());
        returnVo.setRowIndex(paramVo.getNowRow());
        returnVo.setSheetIndex(paramVo.getNowSheet());
        return returnVo;
    }

    default Map<String,Object> buildResult(ExcelReturnVo excelReturnVo) {
        Map<String,Object> map = new HashMap<>();
        map.put("code",excelReturnVo.getCode());
        map.put("msg",excelReturnVo.getMsg());
        map.put("sheet",excelReturnVo.getSheetIndex());
        return map;
    }

}
