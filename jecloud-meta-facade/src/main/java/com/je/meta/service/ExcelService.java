/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.meta.model.ExcelParamVo;
import com.je.meta.model.ExcelReturnVo;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-08-05 17:04
 * @description:
 */
public interface ExcelService {

    /**
     * 格式化数据
     */
    public List<List<Object>> buildExportData(String funcId, List<DynaBean> datas, Map<String, Map<String, String>> ddInfos, List<DynaBean> formColumns);
    /**
     * 核心表 - 后续补充
     */
    List<String> CORE_TABLE = Lists.newArrayList("JE_CORE_DICTIONARY");

    /**
     * 得到当前索引列的列名  A B
     *
     * @param col
     * @return
     */
    default String getExcelColumnName(int col) {
        StringBuilder str = new StringBuilder(2);
        setExcelColumnName(str, col);
        return str.toString();
    }

    default void setExcelColumnName(StringBuilder str, int col) {
        int tmp = col / 26;
        if (tmp > 26) {
            setExcelColumnName(str, tmp - 1);
        } else if (tmp > 0) {
            str.append((char) (tmp + 64));
        }
        str.append((char) (col % 26 + 65));
    }

    default Map<String, String> getRequestParams(HttpServletRequest request) {
        Enumeration ener = request.getParameterNames();
        Map<String, String> params = new HashMap<String, String>();
        while (ener.hasMoreElements()) {
            String key = ener.nextElement() + "";
            params.put(key, request.getParameter(key));
        }
        return params;
    }

    /**
     * 导出Excel
     *
     * @return java.io.ByteArrayOutputStream
     * @throws Exception 异常
     */
    InputStream exportExcel(JSONObject params, HttpServletRequest request);

    JSONObject impData(DynaBean groupTemBean, String fileKey, HttpServletRequest request) throws IOException;

    JSONObject impData(String code, String fileKey, HttpServletRequest request);

    /**
     * 导入数据
     *
     * @param code       编码
     * @param filePath   文件唯一标识
     * @param groupTemId Excel数据导入临时组ID
     * @param request    TODO未处理
     * @return net.sf.json.JSONObject
     */
    JSONObject impData(String code, String filePath, String groupTemId, HttpServletRequest request) throws IOException;

    /**
     * 导入预处理数据
     *
     * @param code      编码
     * @param temIds    TODO未处理
     * @param request   TODO未处理
     * @param returnObj TODO未处理
     */
    void impPreviewData(String code, String temIds, HttpServletRequest request, JSONObject returnObj);

    /**
     * 执行前后逻辑
     *
     * @param type           TODO未处理
     * @param sql            TODO未处理
     * @param datasourceName TODO未处理
     * @param procedureName  TODO未处理
     * @param pkValue        TODO未处理
     * @param queryType      查询类型
     * @param beanName       TODO未处理
     * @param beanMethod     TODO未处理
     * @param paramValues    TODO未处理
     * @param paramVo        TODO未处理
     * @return
     */
    ExcelReturnVo executeLogic(String type, String sql, String datasourceName, String procedureName, String pkValue, String queryType, String beanName, String beanMethod, Map<String, Object> paramValues, ExcelParamVo paramVo);

    /**
     * 处理导入数据
     *
     * @param group
     * @param sheets
     * @param results
     * @param ddSet
     * @param request
     * @param allFieldInfos
     * @return
     */
    List<ExcelReturnVo> doData(DynaBean group, List<DynaBean> sheets, Map<String, List<DynaBean>> results, Map<String, Object> ddSet, HttpServletRequest request, Map<String, Map<String, DynaBean>> allFieldInfos);

    void implExcelFields(DynaBean dynaBean);

    List<Map<String, Object>> funcTemplateListById(String sheetId);
}
