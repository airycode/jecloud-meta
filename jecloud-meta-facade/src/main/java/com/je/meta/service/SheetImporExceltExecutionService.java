/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import com.je.common.base.DynaBean;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface SheetImporExceltExecutionService extends  ScopeImporExceltExecutionService{

    /**
     * 执行前
     * @param dynaBean
     * @param ddMap
     * @param excelParamVo
     * @return
     */
    /*@Override
    ExcelReturnVo executeDataPre(DynaBean dynaBean, Map<String, Object> ddMap, ExcelParamVo excelParamVo);*/

    /**
     * 数据处理中
     */

    List<DynaBean> executeData(DynaBean group, DynaBean sheet, String groupTemId, Map<String, Object> ddMap, List<DynaBean> excelData, HttpServletRequest request,Map<String,Object> result);

    void handleData(DynaBean group,DynaBean sheet,List<Map<Integer,Object>> datas,HttpServletRequest request);
    /**
     * 保存数据到
     */
    void saveData(DynaBean sheet, List<DynaBean> datas);
}
