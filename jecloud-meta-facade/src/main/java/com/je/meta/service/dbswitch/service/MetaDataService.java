/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.service;

import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.service.dbswitch.model.response.MetadataSchemaDetailResponse;
import com.je.meta.service.dbswitch.model.response.MetadataTableDetailResponse;
import com.je.meta.service.dbswitch.model.response.MetadataTableInfoResponse;
import com.je.meta.service.dbswitch.model.response.SchemaTableDataResponse;

import java.util.List;


public interface MetaDataService {

    /**
     * 获取所有库/模式
     *
     * @return
     */
    List<MetadataSchemaDetailResponse> allSchemas();

    Page allTables(String schema, String productId, String fuzzyValue, int intPage, int intLimit);

    /**
     * 查看指定表元数据信息
     *
     * @param schema 库/模式
     * @param table  表名
     * @return
     */
    MetadataTableDetailResponse tableDetail(String schema, String table);

    /**
     * 获取指定表的元数据信息 返回vo
     *
     * @param schema
     * @param tables
     * @return
     */
    List<MetadataTableDetailResponse> tablesDetailVo(String schema, String tables);

    /**
     * 获取指定表的元数据信息 返回bo
     *
     * @param schema 库/模式
     * @param tables 表
     * @return
     */
    void updateTableColumnMeta(String schema, String tables, String syParent, String productId);

    /**
     * 获取指定表的数据
     *
     * @param schema 库/模式
     * @param table  表
     * @return
     */
    SchemaTableDataResponse tableData(String schema, String table);

    /**
     * 导入表
     *
     * @param schema    库模式/库名
     * @param tables    table名
     * @param syParent  导入目录id
     * @param productId 产品id
     * @return
     */
    String importTable(String schema, String tables, String syParent, String productId);

}
