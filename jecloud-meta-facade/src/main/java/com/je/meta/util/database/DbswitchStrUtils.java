/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.util.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 字符串工具类
 *
 * @author tang
 * @date 2021/6/8 20:55
 */
public final class DbswitchStrUtils {

  /**
   * 根据逗号切分字符串为数组
   *
   * @param str 待切分的字符串
   * @return List
   */
  public static List<String> stringToList(String str) {
    if (null != str && str.length() > 0) {
      String[] strs = str.split(",");
      if (strs.length > 0) {
        return new ArrayList<>(Arrays.asList(strs));
      }
    }

    return new ArrayList<>();
  }


  /**
   * 将二进制数据转换为16进制的可视化字符串
   *
   * @param bytes 二进制数据
   * @return 16进制的可视化字符串
   */
  public static String toHexString(byte[] bytes) {
    if (null == bytes || bytes.length <= 0) {
      return null;
    }
    final StringBuilder hexString = new StringBuilder();
    for (byte b : bytes) {
      int v = b & 0xFF;
      String s = Integer.toHexString(v);
      if (s.length() < 2) {
        hexString.append(0);
      }
      hexString.append(s);
    }
    return hexString.toString();
  }

  private DbswitchStrUtils() {
  }
}
