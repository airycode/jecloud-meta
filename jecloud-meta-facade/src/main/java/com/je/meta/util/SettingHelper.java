/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util;

import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.meta.util.setting.Open.OpenDd;
import com.je.meta.util.setting.Open.OpenFeiSu;
import com.je.meta.util.setting.Open.OpenWeChat;
import com.je.meta.util.setting.Open.OpenWeLink;
import com.je.meta.util.setting.document.DocumentOnlyOffice;
import com.je.meta.util.setting.document.DocumentWps;
import com.je.meta.util.setting.login.*;
import com.je.meta.util.setting.push.Note253;
import com.je.meta.util.setting.push.NoteAliyun;
import com.je.meta.util.setting.push.NoteWj;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.StringUtil;

public class SettingHelper {

    private static SettingHelper settingHelper;

    /**
     * 获取only配置
     * @return
     */
    public static DocumentOnlyOffice getOnlyOfficeConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        DocumentOnlyOffice documentOnlyOffice = new DocumentOnlyOffice().build(systemSettingRpcService);
        return documentOnlyOffice;
    }

    /**
     * 获取wps配置
     * @return
     */
    public static DocumentWps getWpsConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        DocumentWps documentWps = new DocumentWps().build(systemSettingRpcService);
        return documentWps;
    }

    /**
     * 获取登录钉钉配置
     * @return
     */
    public static LoginDd getLoginDdConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        LoginDd loginDd = new LoginDd().build(systemSettingRpcService);
        return loginDd;
    }

    /**
     * 获取登录飞书配置
     * @return
     */
    public static LoginFeiSu getLoginFeiSuConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        LoginFeiSu loginFeiSu = new LoginFeiSu().build(systemSettingRpcService);
        return loginFeiSu;
    }

    /**
     * 获取登录QQ配置
     * @return
     */
    public static LoginQq getLoginQqConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        LoginQq loginQq = new LoginQq().build(systemSettingRpcService);
        return loginQq;
    }

    /**
     * 获取登录微信配置
     * @return
     */
    public static LoginWeChat getLoginWeChatConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        LoginWeChat loginWeChat = new LoginWeChat().build(systemSettingRpcService);
        return loginWeChat;
    }

    /**
     * 获取登录WeLink配置
     * @return
     */
    public static LoginWeLink getLoginWeLinkConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        LoginWeLink loginWeLink = new LoginWeLink().build(systemSettingRpcService);
        return loginWeLink;
    }

    /**
     * 获取钉钉开放配置
     * @return
     */
    public static OpenDd getOpenDdConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        OpenDd openDd = new OpenDd().build(systemSettingRpcService);
        return openDd;
    }

    /**
     * 获取飞书开放配置
     * @return
     */
    public static OpenFeiSu getOpenFeiSuConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        OpenFeiSu openFeiSu = new OpenFeiSu().build(systemSettingRpcService);
        return openFeiSu;
    }

    /**
     * 获取微信开放配置
     * @return
     */
    public static OpenWeChat getOpenWeChatConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        OpenWeChat openWeChat = new OpenWeChat().build(systemSettingRpcService);
        return openWeChat;
    }

    /**
     * 获取WeLink开放配置
     * @return
     */
    public static OpenWeLink getOpenWeLinkConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        OpenWeLink openWeLink = new OpenWeLink().build(systemSettingRpcService);
        return openWeLink;
    }

    /**
     * 获取253短信配置
     * @return
     */
    public static Note253 getNote253Config(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        Note253 note253 = new Note253().build(systemSettingRpcService);
        return note253;
    }

    /**
     * 获取阿里云短信配置
     * @return
     */
    public static NoteAliyun getNoteAliyunConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        NoteAliyun noteAliyun = new NoteAliyun().build(systemSettingRpcService);
        return noteAliyun;
    }

    /**
     * 获取网建短信配置
     * @return
     */
    public static NoteWj getNoteWjConfig(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        NoteWj noteWj = new NoteWj().build(systemSettingRpcService);
        return noteWj;
    }


    /**
     * 获取开启的office预览方式
     * @return
     */
    public static String getOfficeType(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        return systemSettingRpcService.findSettingValue("JE_SYS_DOCUMENT");
    }

    /**
     * 是否开启异常日志
     * @return
     */
    public static boolean getErrorLogStatus() {
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String loginfo = systemSettingRpcService.findSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(loginfo)){
            return false;
        }
        return loginfo.contains("JE_SYS_EXCEPTION");
    }

    /**
     * 是否开启登录日志
     * @return
     */
    public static boolean getLoginLogStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String loginfo = systemSettingRpcService.findSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(loginfo)){
            return false;
        }
        return loginfo.contains("JE_SYS_LOGINLOG");
    }

    /**
     * 是否开启开发日志
     * @return
     */
    public static boolean getDevLogStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String loginfo = systemSettingRpcService.findSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(loginfo)){
            return false;
        }
        return loginfo.contains("JE_SYS_DEVELOPLOG");
    }

    /**
     * 是否开启执行日志
     * @return
     */
    public static boolean getExecutionLogStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String loginfo = systemSettingRpcService.findSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(loginfo)){
            return false;
        }
        return loginfo.contains("JE_SYS_EXECUITE");
    }

    /**
     * 获取上传文件大小限制
     * @return
     */
    public static String getUploadFileSize(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String uploadfilesize = systemSettingRpcService.findSettingValue("JE_SYS_UPLOADFILESIZE");
        if(StringUtil.isEmpty(uploadfilesize)){
            return "10240";
        }
        return uploadfilesize;
    }

    /**
     * 浏览器数据是否开启加密
     * @return
     */
    public static Boolean getEncryp(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String encrypt = systemSettingRpcService.findSettingValue("JE_SYS_ENCRYPT");
        if(StringUtil.isEmpty(encrypt)){
            return false;
        }
        return encrypt.equals("1")? true:false;
    }

    /**
     * 获取浏览器加密传输的指定字段
     * @return
     */
    public static String getEncrypField(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String encryptField = systemSettingRpcService.findSettingValue("JE_SYS_ENCRYPT_FIELD");
        if(StringUtil.isEmpty(encryptField)){
            return "";
        }
        return encryptField;
    }

    /**
     * 是否开启web页面消息推送
     * @return
     */
    public static boolean getWebMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("COMMON");
    }

    /**
     * 是否开启邮件消息推送
     * @return
     */
    public static boolean getMailMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("EMAIL");
    }

    /**
     * 是否开启短信消息推送
     * @return
     */
    public static boolean getNoteMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("NOTE");
    }

    /**
     * 是否开启APP消息推送
     * @return
     */
    public static boolean getAppMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("APP");
    }

    /**
     * 是否开启企业微信消息推送
     * @return
     */
    public static boolean getWxMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("WX");
    }

    /**
     * 是否开启飞书消息推送
     * @return
     */
    public static boolean getFsMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("FS");
    }

    /**
     * 是否开启钉钉消息推送
     * @return
     */
    public static boolean getDdMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("DD");
    }

    /**
     * 是否开启华为WeLink消息推送
     * @return
     */
    public static boolean getWeLinkMessagePushStatus(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String notediy = systemSettingRpcService.findSettingValue("JE_SYS_NOTEDIY");
        if(StringUtil.isEmpty(notediy)){
            return false;
        }
        return notediy.contains("HW");
    }

    /**
     * 是否允许使用手机验证码登录
     * @return
     */
    public static boolean getPhoneLogin(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String phonelogin = systemSettingRpcService.findSettingValue("JE_CORE_PHONELOGIN");
        if(StringUtil.isEmpty(phonelogin)){
            return false;
        }
        return phonelogin.equals("1")? true:false;
    }

    /**
     *  是否允许使用手机找回密码
     * @return
     */
    public static boolean getFindPasswordByPhone(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String phonerepassword = systemSettingRpcService.findSettingValue("JE_CORE_PHONEREPASSWORD");
        if(StringUtil.isEmpty(phonerepassword)){
            return false;
        }
        return phonerepassword.equals("1")? true:false;
    }

    /**
     * 是否强制用户修改默认密码
     * @return
     */
    public static boolean getIsUpdateDefultPassword(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String initpassword = systemSettingRpcService.findSettingValue("INITPASSWORD");
        if(StringUtil.isEmpty(initpassword)){
            return false;
        }
        return initpassword.equals("1")? true:false;
    }

    /**
     * 获取密码允许失败次数
     * @return
     */
    public static String getPasswordByfail(){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String errorpw = systemSettingRpcService.findSettingValue("JE_CORE_ERRORPW");
        if(StringUtil.isEmpty(errorpw)){
            return "5";
        }
        return errorpw;
    }

    /**
     * 判断当前用户是否是网盘管理员
     * @param userId
     * @return
     */
    public static boolean getDocadminAdmin(String userId){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String docadmin = systemSettingRpcService.findSettingValue("JE_SYS_DOCADMIN");
        if(StringUtil.isEmpty(docadmin)){
            return false;
        }
        return docadmin.contains(userId);
    }

    /**
     * 判断用户是否是开发团队成员
     * @param userId
     * @return
     */
    public static boolean getAdmin(String userId){
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        String admin = systemSettingRpcService.findSettingValue("JE_SYS_ADMIN");
        if(StringUtil.isEmpty(admin)){
            return false;
        }
        return admin.contains(userId);
    }

}
