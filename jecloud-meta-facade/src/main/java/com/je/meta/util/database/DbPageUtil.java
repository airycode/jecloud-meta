/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.database;

import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.service.dbswitch.model.response.MetadataTableInfoResponse;

import java.util.ArrayList;
import java.util.List;

public class DbPageUtil {
    /**
     * 内存分页
     *
     * @param pagingList
     * @param pageNum
     * @param pageSize
     * @return
     */
    public static <E> Page<E> getPage(List<E> pagingList, int pageNum, int pageSize) {
        Page resultPage = new Page(pageNum, pageSize);
        resultPage.setTotal(pagingList.size());
        resultPage.setRecords(subList(pagingList, pageNum, pageSize));
        return resultPage;
    }

    private static <E> List<E> subList(List<E> list, int pageNum, int pageSize) {
        int size = list.size();
        List<E> result = new ArrayList<>();
        int idx = (pageNum - 1) * pageSize;
        int end = idx + pageSize;
        while (idx < size && idx < end) {
            result.add(list.get(idx));
            idx++;
        }
        return result;
    }
}
