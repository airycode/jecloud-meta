/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.setting.push;

import com.je.common.base.service.rpc.SystemSettingRpcService;

public class Note253 {

    private String account;

    private String password;

    private String report;

    private String signName;

    private String template;

    private String url;

    public Note253 build(SystemSettingRpcService systemSettingRpcService) {
        account = systemSettingRpcService.findSettingValue("NOTE_253_USERNAME");
        password = systemSettingRpcService.findSettingValue("NOTE_253_PASSWORD");
        report = systemSettingRpcService.findSettingValue("NOTE_253_REPORT");
        signName = systemSettingRpcService.findSettingValue("NOTE_253_SIGNNAME");
        template = systemSettingRpcService.findSettingValue("NOTE_253_TEMPLATE");
        url = systemSettingRpcService.findSettingValue("NOTE_253_SENDURL");
        return this;
    }

    public Note253() {
    }

    public Note253(String account, String password, String report, String signName, String template, String url) {
        this.account = account;
        this.password = password;
        this.report = report;
        this.signName = signName;
        this.template = template;
        this.url = url;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
