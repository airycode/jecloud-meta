/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.database.type;

public enum ColumnTypeEnum {
    /**
     * 自定义
     */
    CUSTOM,
    /**
     * 超大文本
     */
    CLOB,
    /**
     * 字符串(767)
     */
    VARCHAR767,
    /**
     * 字符串(1000)
     */
    VARCHAR1000,
    /**
     * 字符串(2000)
     */
    VARCHAR2000,
    /**
     * 整数
     */
    NUMBER,
    /**
     * 是非
     */
    YESORNO,
    /**
     * 小数
     */
    FLOAT,
    /**
     * 日期时间
     */
    DATETIME,
    /**
     * 字符串(4000)
     */
    VARCHAR4000,
    /**
     * 字符串(30)
     */
    VARCHAR30,
    /**
     * 字符串(255)
     */
    VARCHAR255,
    /**
     * 字符串
     */
    VARCHAR,
    /**
     * 字符串(100)
     */
    VARCHAR100,
    /**
     * 外键
     */
    FOREIGNKEY,
    /**
     * 主键
     */
    ID,
    /**
     * 日期
     */
    DATE,
    /**
     * 小数(2)
     */
    FLOAT2,
    /**
     * 巨大文本
     */
    BIGCLOB,
    /**
     * 字符串(50)
     */
    VARCHAR50,
}
