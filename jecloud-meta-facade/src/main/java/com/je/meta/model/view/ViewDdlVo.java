/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.view;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import java.util.ArrayList;
import java.util.List;

public class ViewDdlVo {
    /**
     * 视图code
     */
    private String viewCode;
    /**
     * 主表
     */
    private String masterTableId;
    /**
     * 视图关系
     */
    private List<ViewColumn> viewColumn;
    /**
     * 视图字段
     */
    private List<ViewOuput> viewOuput;

    /**
     * 查询配置
     */
    private QueryConfigVo queryConfigVo;


    public QueryConfigVo getQueryConfigVo() {
        return queryConfigVo;
    }

    public void setQueryConfigVo(QueryConfigVo queryConfigVo) {
        this.queryConfigVo = queryConfigVo;
    }

    /**
     * 检查视图输出字段是否重复，null为不重复
     *
     * @param viewOuputList
     * @return
     */
    public static String checkRepeatField(List<ViewOuput> viewOuputList) {
        StringBuffer sb = new StringBuffer();
        if (viewOuputList != null && viewOuputList.size() > 0) {
            List<String> list = new ArrayList<>();
            for (ViewOuput viewOuput : viewOuputList) {
                if (list.contains(viewOuput.getColumn().trim())) {
                    sb.append(viewOuput.getColumn().trim() + ",");
                    continue;
                }
                list.add(viewOuput.getColumn().trim());
            }
        }
        if (sb.length() > 0) {
            return sb.substring(0, sb.length() - 1);
        }
        return null;
    }

    public static ViewDdlVo build(String strData) {
        ViewDdlVo viewDdlVo = new ViewDdlVo();
        JSONObject jsonObject = JSON.parseObject(strData);

        String viewCode = jsonObject.getString("viewCode");
        if (!Strings.isNullOrEmpty(viewCode)) {
            viewDdlVo.setViewCode(viewCode);
        } else {
            throw new PlatformException("视图编码为空,创建失败！", PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }

        String masterTableId = jsonObject.getString("masterTableId");
        if (!Strings.isNullOrEmpty(masterTableId)) {
            viewDdlVo.setMasterTableId(masterTableId);
        } else {
            throw new PlatformException("主表编码为空,创建失败！", PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }

        String viewColumn = jsonObject.getString("viewColumn");
        viewDdlVo.setViewColumn(ViewColumn.build(viewColumn));

        String viewOuput = jsonObject.getString("viewOuput");
        viewDdlVo.setViewOuput(ViewOuput.build(viewOuput));

        return viewDdlVo;
    }

    public String getViewCode() {
        return viewCode;
    }

    public void setViewCode(String viewCode) {
        this.viewCode = viewCode;
    }

    public String getMasterTableId() {
        return masterTableId;
    }

    public void setMasterTableId(String masterTableId) {
        this.masterTableId = masterTableId;
    }

    public List<ViewColumn> getViewColumn() {
        return viewColumn;
    }

    public void setViewColumn(List<ViewColumn> viewColumn) {
        this.viewColumn = viewColumn;
    }

    public List<ViewOuput> getViewOuput() {
        return viewOuput;
    }

    public void setViewOuput(List<ViewOuput> viewOuput) {
        this.viewOuput = viewOuput;
    }

}

