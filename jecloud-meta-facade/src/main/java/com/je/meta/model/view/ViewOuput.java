/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.view;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 输出
 */
public class ViewOuput {

    private String tableName;

    private String formula;

    private boolean grouping;

    private String sort;

    private String tableCode;

    private String column;

    private String value;

    private String name;

    private String dataType;

    private String length;

    private String whereColum;

    private int orderIndex;

    private String type;

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static List<ViewOuput> build(String viewOuputStr) {
        JSONArray viewOuputJson = new JSONArray();
        List<ViewOuput> list = new ArrayList<>();

        if (Strings.isNullOrEmpty(viewOuputStr)) {
            return list;
        }

        try {
            viewOuputJson = JSON.parseArray(viewOuputStr);
        } catch (Exception e) {
            throw new PlatformException("解析视图关系信息异常！", PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }

        for (int i = 0; i < viewOuputJson.size(); i++) {
            JSONObject jsonObject = viewOuputJson.getJSONObject(i);
            ViewOuput viewOuput = new ViewOuput();
            viewOuput.setTableName(jsonObject.getString("tableName"));
            viewOuput.setValue(jsonObject.getString("value"));
            viewOuput.setTableCode(jsonObject.getString("tableCode"));
            if (Strings.isNullOrEmpty(jsonObject.getString("column"))) {
                throw new PlatformException(String.format("创建失败，%s列编码信息为空，请检查！", jsonObject.getString("name"))
                        , PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
            }
            viewOuput.setColumn(jsonObject.getString("column"));
            viewOuput.setName(jsonObject.getString("name"));
            viewOuput.setDataType(jsonObject.getString("dataType"));
            String grouping = jsonObject.getString("grouping");
            if (!Strings.isNullOrEmpty(grouping) && (grouping.equals("true") || grouping.equals("1"))) {
                viewOuput.setGrouping(true);
            } else {
                viewOuput.setGrouping(false);
            }
            viewOuput.setSort(jsonObject.getString("sort"));
            viewOuput.setFormula(jsonObject.getString("formula"));
            viewOuput.setWhereColum(jsonObject.getString("whereColum"));
            viewOuput.setType(jsonObject.getString("type"));
            list.add(viewOuput);
        }
        return list;
    }

    public static String toString(ViewOuput v, String masterTableId) {
        if ("ID".equals(v.getDataType()) && !v.getColumn().equals(masterTableId)) {
            v.setDataType("VARCHAR");
        }
        StringBuffer sb = new StringBuffer("{");
        sb.append(String.format("\"tableName\":\"%s\",", v.getTableName()==null?"":v.getTableName()));
        sb.append(String.format("\"tableCode\":\"%s\",", v.getTableCode()));
        sb.append(String.format("\"name\":\"%s\",", v.getName()));
        sb.append(String.format("\"column\":\"%s\",", v.getColumn()));
        sb.append(String.format("\"value\":\"%s\",", v.getValue()));
        sb.append(String.format("\"dataType\":\"%s\",", v.getDataType()));
        sb.append(String.format("\"length\":\"%s\",", v.getLength()==null?"":v.getLength()));
        sb.append(String.format("\"formula\":\"%s\",", v.getFormula()));
        sb.append(String.format("\"grouping\":%s,", v.isGrouping()));
        sb.append(String.format("\"sort\":\"%s\",", v.getSort()));
        sb.append(String.format("\"type\":\"%s\",", v.getType()));
        sb.append(String.format("\"whereColum\":\"%s\"", v.getWhereColum()));
        sb.append("}");
        return sb.toString();
    }

    public static Map<String, ViewOuput> buildViewOuPutListToMap(List<ViewOuput> list) {
        Map<String, ViewOuput> ouPutMap = new HashMap<>();
        for(int i =0;i<list.size();i++){
            ViewOuput v = list.get(i);
            v.setOrderIndex(i+1);
            ouPutMap.put(v.getColumn(), v);
        }
        return ouPutMap;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public boolean isGrouping() {
        return grouping;
    }

    public void setGrouping(boolean grouping) {
        this.grouping = grouping;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWhereColum() {
        return whereColum;
    }

    public void setWhereColum(String whereColum) {
        this.whereColum = whereColum;
    }
}
