/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.func;

import java.io.Serializable;

public class FuncColumnTacticInfo implements Serializable {
    private static final long serialVersionUID = -3349409965912818578L;
    //-----------------------字典渲染器---------------------
    /**字典项配置*/
    private String DDCode;
    /**颜色策略配置[通用]*/
    private String color;
    /**是否是从数据字典表中取得的数据*/
    private Boolean ISDD;
    /**日期格式化控件*/
    private String format;
    private String linkMethod;
    //----------------------高亮显示器---------------------
    /**关键字组建名称*/
    private String keyWord;
    public String getDDCode() {
        return DDCode;
    }
    public void setDDCode(String dDCode) {
        DDCode = dDCode;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Boolean getISDD() {
        return ISDD;
    }
    public void setISDD(Boolean iSDD) {
        ISDD = iSDD;
    }
    public String getKeyWord() {
        return keyWord;
    }
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
    public String getLinkMethod() {
        return linkMethod;
    }
    public void setLinkMethod(String linkMethod) {
        this.linkMethod = linkMethod;
    }
    public String getFormat() {
        return format;
    }
    public void setFormat(String format) {
        this.format = format;
    }

}
