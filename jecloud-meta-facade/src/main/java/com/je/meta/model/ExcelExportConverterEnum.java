/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

import com.je.meta.rpc.excel.ExcelExportConverter;
import com.je.meta.service.excel.converter.*;

public enum ExcelExportConverterEnum {
    /**
     * 1.文本框
     */
    TEXT_FIELD("textfield"),
    /**
     * 2.数值框
     */
    NUMBER_FIELD("numberfield"),
    /**
     * 3.单选框
     */
    R_GROUP("rgroup"),
    /**
     * 4.复选框
     */
    C_GROUP("cgroup"),
    /**
     * 5.下拉框
     */
    CBB_FIELD("cbbfield"),
    /**
     * 6.文本域
     */
    TEXT_AREA("textarea"),
    /**
     * 7.HTML编辑器
     */
    CK_EDITOR("ckeditor"),
    /**
     * 8.编号
     */
    TEXT_CODE("textcode"),
    /**
     * 9.附件
     */
    UX_FILE_FIELD("uxfilefield"),
    /**
     * 10.多附件
     */
    UX_FILES_FIELD("uxfilesfield"),
    /**
     * 11.日期
     */
    DATE_FIELD("datefield"),
    /**
     * 12.月份日期
     */
    DATE_MONTH_DATE_FILED("datemonthdatefiled"),
    /**
     * 13.日期时间
     */
    DATE_TIME_FIELD("datetimefield"),
    /**
     * 14.日期月份
     */
    DATE_MONTH_FIELD("datemonthfield"),
    /**
     * 15.日期区间
     */
    RANGE_DATE_FIELD("rangedatefield"),
    /**
     * 16.日期年份
     */
    YEAR_FIELD("yearfield"),
    /**
     * 17.时间
     */
    CLOCK_TIME_FIELD("clocktimefield"),
    /**
     * 18.树形选择
     */
    TREE_SS_FIELD("treessfield"),
    /**
     * 19.树形选择(大)
     */
    TREE_SS_AREA_FIELD("treessareafield"),
    /**
     * 20.查询选择
     */
    GRID_SS_FIELD("gridssfield"),
    /**
     * 21.查询选择(大)
     */
    GRID_SS_AREA_FIELD("gridssareafield"),
    /**
     * 22.智能查询
     */
    SEARCH_FIELD("searchfield"),
    /**
     * 23.人员构造器
     */
    QUERY_USER_FIELD("queryuserfield"),
    /**
     * 24.人员构造器(大)
     */
    QUERY_USER_AREA_FIELD("queryuserareafield"),
    /**
     * 25.下拉框集合
     */
    CBB_LIST_FIELD("cbblistfield"),
    /**
     * 26.多数据项集合
     */
    MULTI_ITEM("multiitem"),
    /**
     * 27.子功能集合 不会展示在列表
     */
    CHILD_FUNC_FIELD("childfuncfield"),
    /**
     * 28.颜色选择器
     */
    COLOR_FIELD("colorfield"),
    /**
     * 29.流程图字段
     */
    GRAPH_FIELD("graphfield"),
    /**
     * 30.展示字段 不会展示在列表
     */
    DISPLAY_FIELD("displayfield"),
    /**
     * 31.评星
     */
    STAR_FIELD("starfield"),
    /**
     * 32.拼音
     */
    PINYIN_FIELD("pinyinfield"),
    /**
     * 33.进度条
     */
    BAR_FIELD("barfield"),
    /**
     * 34.子功能 不会展示在列表
     */
    CHILD("child"),
    /**
     * 35.分组框 不会展示在列表
     */
    FIELDSET("fieldset"),
    /**
     * 36.新人员选择
     */
    VUE_USER_FIELD("vueuserfield"),
    /**
     * 37.新人员选择(大)
     */
    VUE_USER_AREA_FIELD("vueuserareafield"),
    /**
     * 38.数据集合
     */
    JSON_ARRAY_FIELD("jsonarrayfield"),
    /**
     * 39.图片选择器
     */
    IMAGE_PICKER_FIELD("imagepickerfield"),
    /**
     * 40.代码编辑器
     */
    CODE_EDITOR("codeeditor");

    String value;

    ExcelExportConverterEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }


    /**
     * 根据类型获取枚举
     *
     * @param type 类型对象
     * @return ExcelExportConverter
     */
    public static ExcelExportConverter getExportType(String type) {
        //TEXT
        if (type.toLowerCase().equals(ExcelExportConverterEnum.TEXT_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.TEXT_CODE.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //数字
        if (type.toLowerCase().equals(ExcelExportConverterEnum.NUMBER_FIELD.getValue().toLowerCase())) {
            return new ExcelExportNumberConverter();
        }
        //下拉，单选，多选
        if (type.toLowerCase().equals(ExcelExportConverterEnum.R_GROUP.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.C_GROUP.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.CBB_FIELD.getValue().toLowerCase())) {
            return new ExcelExportChooseConverter();
        }
        //文本域
        if (type.toLowerCase().equals(ExcelExportConverterEnum.TEXT_AREA.getValue().toLowerCase())) {
            return new ExcelExportTextAreaConverter();
        }
        //HTML编辑器
        if (type.toLowerCase().equals(ExcelExportConverterEnum.CK_EDITOR.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.CODE_EDITOR.getValue().toLowerCase())) {
            return new ExcelExportHtmlConverter();
        }
        //附件
        if (type.toLowerCase().equals(ExcelExportConverterEnum.UX_FILE_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.UX_FILES_FIELD.getValue().toLowerCase())) {
            return new ExcelExportFileConverter();
        }
        //时间相关
        if (type.toLowerCase().equals(ExcelExportConverterEnum.DATE_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.DATE_MONTH_DATE_FILED.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.DATE_TIME_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.DATE_MONTH_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.RANGE_DATE_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.CLOCK_TIME_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.YEAR_FIELD.getValue().toLowerCase())) {
            return new ExcelExportTimeConverter();
        }
        //树形选择 查询选择 智能查询 人员构造器
        if (type.toLowerCase().equals(ExcelExportConverterEnum.TREE_SS_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.TREE_SS_AREA_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.GRID_SS_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.GRID_SS_AREA_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.SEARCH_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.QUERY_USER_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.VUE_USER_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.VUE_USER_AREA_FIELD.getValue().toLowerCase()) ||
                type.toLowerCase().equals(ExcelExportConverterEnum.QUERY_USER_AREA_FIELD.getValue().toLowerCase())) {
            return new ExcelExportQuerySelectionConverter();
        }
        //下拉框集合
        if (type.toLowerCase().equals(ExcelExportConverterEnum.CBB_LIST_FIELD.getValue().toLowerCase())) {
            return new ExcelExportDropDownBoxCollectionConverter();
        }
        //多数据项集合
        if (type.toLowerCase().equals(ExcelExportConverterEnum.MULTI_ITEM.getValue().toLowerCase())) {
            return new ExcelExportMultipleDataConverter();
        }
        //颜色选择器
        if (type.toLowerCase().equals(ExcelExportConverterEnum.COLOR_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //流程图
        if (type.toLowerCase().equals(ExcelExportConverterEnum.GRAPH_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //评星
        if (type.toLowerCase().equals(ExcelExportConverterEnum.STAR_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //拼音
        if (type.toLowerCase().equals(ExcelExportConverterEnum.PINYIN_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //进度条
        if (type.toLowerCase().equals(ExcelExportConverterEnum.BAR_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        //数据集合
        if (type.toLowerCase().equals(ExcelExportConverterEnum.JSON_ARRAY_FIELD.getValue().toLowerCase())) {
            return new ExcelExportStringConverter();
        }
        return new ExcelExportStringConverter();
    }

}
