/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

import com.google.common.base.Strings;

public enum ExcelEnum {
    /**
     * 导出“打印样式”
     */
    REPORT("excelRpcReportExportService"),
    /**
     * 导出“表格样式”
     */
    GRID("excelRpcGridExportService"),
    /**
     * 导出“模板样式”
     */
    TEMPLATE("excelRpcTemplateExportService");

    String service;

    ExcelEnum(String service) {
        this.service = service;
    }

    public String getService() {
        return this.service;
    }


    /**
     * 根据类型获取枚举
     *
     * @param type 类型对象
     * @return
     */
    public static ExcelEnum getExportType(String type) {
        if(Strings.isNullOrEmpty(type)){
            return ExcelEnum.GRID;
        }
        if (type.toLowerCase().equals(ExcelEnum.REPORT.name().toLowerCase())) {
            return ExcelEnum.REPORT;
        } else if (type.toLowerCase().equals(ExcelEnum.TEMPLATE.name().toLowerCase())) {
            return ExcelEnum.TEMPLATE;
        } else if (type.toLowerCase().equals(ExcelEnum.GRID.name().toLowerCase())) {
            return ExcelEnum.GRID;
        }
        return null;
    }


}
