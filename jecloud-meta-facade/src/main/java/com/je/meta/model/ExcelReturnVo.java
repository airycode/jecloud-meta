/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

import com.je.common.base.DynaBean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Excel输出信息
 * @author zhangshuaipeng
 *
 */
public class ExcelReturnVo implements Serializable {
    private static final long serialVersionUID = 7502453086490239896L;
    /**
     * 执行操作   全局执行前 ALL_BEFORE  SHEET执行前 SHEET_BEFORE  SHEET单条执行前 SHEET_ONE_BEFORE  数据处理  SHEET_DATA 单条数据处理 SHEET_ONE_DATA SHEET执行后 SHEET_AFTER 全局数据处理 ALL_DATA  全局执行后  ALL_AFTER
     */
    private String doType;
    /**
     * 当前工作簿位置
     */
    private Integer sheetIndex;
    /**
     * 当前行位置
     */
    private Integer rowIndex;
    /**
     * 状态码
     * 取值参考 ExcelErrorCode
     * 1 成功 -1警告 -2 错误 -3 中端异常
     */
    private int code;
    /**
     * 状态信息
     */
    private String msg;

    /**
     * @param code
     * @param msg
     */
    private Map<String, List<Map<Integer,Object>>> allValues;

    /**
     * 每个sheet页的数据
     * @param code
     * @param msg
     */
    private List<DynaBean> sheetValues;

    /**
     * 当前行数据
     * @return
     */
    private DynaBean dynaBean;

    public ExcelReturnVo() {
    }

    public DynaBean getDynaBean() {
        return dynaBean;
    }

    public void setDynaBean(DynaBean dynaBean) {
        this.dynaBean = dynaBean;
    }


    public ExcelReturnVo(int code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getDoType() {
        return doType;
    }
    public void setDoType(String doType) {
        this.doType = doType;
    }
    public Integer getSheetIndex() {
        return sheetIndex;
    }
    public void setSheetIndex(Integer sheetIndex) {
        this.sheetIndex = sheetIndex;
    }
    public Integer getRowIndex() {
        return rowIndex;
    }
    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public Map<String, List<Map<Integer, Object>>> getAllValues() {
        return allValues;
    }

    public void setAllValues(Map<String, List<Map<Integer, Object>>> allValues) {
        this.allValues = allValues;
    }

    public List<DynaBean> getSheetValues() {
        return sheetValues;
    }

    public void setSheetValues(List<DynaBean> sheetValues) {
        this.sheetValues = sheetValues;
    }
}
