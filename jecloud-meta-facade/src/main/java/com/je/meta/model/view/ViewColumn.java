/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.view;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import java.util.ArrayList;
import java.util.List;

public class ViewColumn {

    private String mainTableCode;

    private String targetTableCode;

    private String mainColumn;

    private String targetColumn;

    private String formula;

    private String mainIdentifier;

    private String targetIdentifier;

    private String isDispalyLine;

    private String mainTableId;

    private String mainTableName;

    private String targetTableId;

    private String targetTableName;

    public static List<ViewColumn> build(String viewColumnStr) {
        JSONArray viewColumnJsons = new JSONArray();
        List<ViewColumn> list = new ArrayList<>();
        if (Strings.isNullOrEmpty(viewColumnStr)) {
            return list;
        }
        try {
            viewColumnJsons = JSON.parseArray(viewColumnStr);
        } catch (Exception e) {
            throw new PlatformException("解析视图关系信息异常！", PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }

        for (int i = 0; i < viewColumnJsons.size(); i++) {
            JSONObject jsonObject = viewColumnJsons.getJSONObject(i);
            ViewColumn viewColumn = new ViewColumn();
            viewColumn.setMainTableCode(jsonObject.getString("mainTableCode"));
            viewColumn.setMainColumn(jsonObject.getString("mainColumn"));
            viewColumn.setMainIdentifier(jsonObject.getString("mainIdentifier"));
            viewColumn.setTargetTableCode(jsonObject.getString("targetTableCode"));
            viewColumn.setTargetColumn(jsonObject.getString("targetColumn"));
            viewColumn.setTargetIdentifier(jsonObject.getString("targetIdentifier"));
            viewColumn.setFormula(jsonObject.getString("formula"));
            viewColumn.setIsDispalyLine(jsonObject.getString("isDispalyLine"));
            viewColumn.setMainTableId(jsonObject.getString("mainTableId"));
            viewColumn.setMainTableName(jsonObject.getString("mainTableName"));
            viewColumn.setTargetTableId(jsonObject.getString("targetTableId"));
            viewColumn.setTargetTableName(jsonObject.getString("targetTableName"));
            list.add(viewColumn);
        }
        return list;
    }


    public String getMainTableCode() {
        return mainTableCode;
    }

    public void setMainTableCode(String mainTableCode) {
        this.mainTableCode = mainTableCode;
    }

    public String getTargetTableCode() {
        return targetTableCode;
    }

    public void setTargetTableCode(String targetTableCode) {
        this.targetTableCode = targetTableCode;
    }

    public String getMainColumn() {
        return mainColumn;
    }

    public void setMainColumn(String mainColumn) {
        this.mainColumn = mainColumn;
    }

    public String getTargetColumn() {
        return targetColumn;
    }

    public void setTargetColumn(String targetColumn) {
        this.targetColumn = targetColumn;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getMainIdentifier() {
        return mainIdentifier;
    }

    public void setMainIdentifier(String mainIdentifier) {
        this.mainIdentifier = mainIdentifier;
    }

    public String getTargetIdentifier() {
        return targetIdentifier;
    }

    public void setTargetIdentifier(String targetIdentifier) {
        this.targetIdentifier = targetIdentifier;
    }

    public String getIsDispalyLine() {
        return isDispalyLine;
    }

    public void setIsDispalyLine(String isDispalyLine) {
        this.isDispalyLine = isDispalyLine;
    }

    public String getMainTableId() {
        return mainTableId;
    }

    public void setMainTableId(String mainTableId) {
        this.mainTableId = mainTableId;
    }

    public String getMainTableName() {
        return mainTableName;
    }

    public void setMainTableName(String mainTableName) {
        this.mainTableName = mainTableName;
    }

    public String getTargetTableId() {
        return targetTableId;
    }

    public void setTargetTableId(String targetTableId) {
        this.targetTableId = targetTableId;
    }

    public String getTargetTableName() {
        return targetTableName;
    }

    public void setTargetTableName(String targetTableName) {
        this.targetTableName = targetTableName;
    }
}
