/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.model.database;


import com.je.common.base.constants.table.TableType;

/**
 * 数据库表描述符信息定义(Table Description)
 *
 * @author tang
 */
public class TableDescription {

    private String tableCode;
    private String tableName;
    private String schemaName;
    private String tableType;
    /**
     * 元数据是否创建
     */
    private String metaIsCreate;
    /**
     * 主键生成策略_NAME
     */
    private String keyGeneratorName;
    /**
     * 主键生成策略_CODE
     */
    private String keyGeneratorType;
    /**
     * 主键生成查询sql
     */
    private String keyGeneratorSql;
    /**
     * 序列名称
     */
    private String incrementerName;

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getMetaIsCreate() {
        return metaIsCreate;
    }

    public void setMetaIsCreate(String metaIsCreate) {
        this.metaIsCreate = metaIsCreate;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        if (tableType.equals("TABLE")) {
            this.tableType = TableType.PTTABLE;
        } else {
            this.tableType = tableType;
        }
    }

    public boolean isViewTable() {
        return TableType.VIEWTABLE.equals(tableType);
    }

    public String getKeyGeneratorName() {
        return keyGeneratorName;
    }

    public void setKeyGeneratorName(String keyGeneratorName) {
        this.keyGeneratorName = keyGeneratorName;
    }

    public String getKeyGeneratorType() {
        return keyGeneratorType;
    }

    public void setKeyGeneratorType(String keyGeneratorType) {
        this.keyGeneratorType = keyGeneratorType;
    }

    public String getKeyGeneratorSql() {
        return keyGeneratorSql;
    }

    public void setKeyGeneratorSql(String keyGeneratorSql) {
        this.keyGeneratorSql = keyGeneratorSql;
    }

    public String getIncrementerName() {
        return incrementerName;
    }

    public void setIncrementerName(String incrementerName) {
        this.incrementerName = incrementerName;
    }
}
