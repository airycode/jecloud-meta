/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.model.database;

import java.util.List;

public class SchemaTableMeta extends TableDescription {

    private List<String> primaryKeys;
    private List<IndexesData> indexesData;
    private List<ImportedKeyData> importedKeyData;
    private String createSql;
    private List<ColumnDescription> columns;
    /**
     * 所属父表
     */
    private String parentTableCodes;

    public String getCreateSql() {
        return createSql;
    }

    public void setCreateSql(String createSql) {
        this.createSql = createSql;
    }

    public List<ColumnDescription> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnDescription> columns) {
        this.columns = columns;
    }

    public List<String> getPrimaryKeys() {
        return primaryKeys;
    }

    public void setPrimaryKeys(List<String> primaryKeys) {
        this.primaryKeys = primaryKeys;
    }

    public List<ImportedKeyData> getImportedKeyData() {
        return importedKeyData;
    }

    public void setImportedKeyData(List<ImportedKeyData> importedKeyData) {
        this.importedKeyData = importedKeyData;
    }

    public List<IndexesData> getIndexesData() {
        return indexesData;
    }

    public void setIndexesData(List<IndexesData> indexesData) {
        this.indexesData = indexesData;
    }

    public String getParentTableCodes() {
        return parentTableCodes;
    }

    public void setParentTableCodes(String parentTableCodes) {
        this.parentTableCodes = parentTableCodes;
    }


}
