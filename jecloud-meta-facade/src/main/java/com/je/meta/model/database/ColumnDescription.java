/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.model.database;


import com.je.meta.model.database.type.ProductTypeEnum;

/**
 * 数据库列描述符信息定义(Column Description)
 *
 * @author tang
 */
public class ColumnDescription {

    /**
     * 字段编码
     */
    private String fieldName;
    /**
     * 字段编码
     */
    private String labelName;
    /**
     * 字段类型
     */
    private String fieldTypeName;
    /**
     * 字段类型
     */
    private String filedTypeClassName;
    /**
     * 类型int
     */
    private int fieldType;
    /**
     * 长度
     */
    private int displaySize;
    /**
     * 比例大小
     */
    private int scaleSize;
    /**
     * 精度大小
     */
    private int precisionSize = 0;
    /**
     * 自动增量
     */
    private boolean autoIncrement;
    /**
     * 可为空的
     */
    private boolean nullable;
    /**
     * 备注信息
     */
    private String remarks;
    /**
     * 签署
     */
    private boolean signed = false;
    /**
     * 数据库类型
     */
    private ProductTypeEnum productType;

    public String getFieldName() {
        if (null != this.fieldName) {
            return fieldName;
        }

        return this.labelName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getLabelName() {
        if (null != labelName) {
            return labelName;
        }

        return this.fieldName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getFieldTypeName() {
        return fieldTypeName;
    }

    public void setFieldTypeName(String fieldTypeName) {
        this.fieldTypeName = fieldTypeName;
    }

    public String getFiledTypeClassName() {
        return filedTypeClassName;
    }

    public void setFiledTypeClassName(String filedTypeClassName) {
        this.filedTypeClassName = filedTypeClassName;
    }

    public int getFieldType() {
        return fieldType;
    }

    public void setFieldType(int fieldType) {
        this.fieldType = fieldType;
    }

    public int getDisplaySize() {
        return displaySize;
    }

    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
    }

    public int getScaleSize() {
        return scaleSize;
    }

    public void setScaleSize(int scaleSize) {
        this.scaleSize = scaleSize;
    }

    public int getPrecisionSize() {
        return precisionSize;
    }

    public void setPrecisionSize(int precisionSize) {
        this.precisionSize = precisionSize;
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean isNullable) {
        this.nullable = isNullable;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public ProductTypeEnum getProductType() {
        return this.productType;
    }

    public void setProductType(ProductTypeEnum productType) {
        this.productType = productType;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ColumnDescription copy() {
        ColumnDescription description = new ColumnDescription();
        description.setFieldName(fieldName);
        description.setLabelName(labelName);
        description.setFieldTypeName(fieldTypeName);
        description.setFiledTypeClassName(filedTypeClassName);
        description.setFieldType(fieldType);
        description.setDisplaySize(displaySize);
        description.setScaleSize(scaleSize);
        description.setPrecisionSize(precisionSize);
        description.setAutoIncrement(autoIncrement);
        description.setNullable(nullable);
        description.setRemarks(remarks);
        description.setSigned(signed);
        description.setProductType(productType);
        return description;
    }

    /////////////////////////////////////////////

    public ColumnMetaData getMetaData() {
        return new ColumnMetaData(this);
    }

}
