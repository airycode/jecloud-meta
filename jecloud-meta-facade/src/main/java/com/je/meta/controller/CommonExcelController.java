/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.rpc.DocumentInternalRpcService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.CommonExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/meta/commonExcel")
public class CommonExcelController extends AbstractPlatformController {

    @Autowired
    private MetaResourceService metaResourceService;

    @Autowired
    private DocumentInternalRpcService documentInternalRpcService;

    @Autowired
    private CommonExcelService excelService;

    /**
     * 无窗口直接导入数据
     */
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult uploadFile(HttpServletRequest request) {
        //获取文件唯一标识
        String fileKey = getStringParameter(request,"fileKey");
        if (StringUtil.isNotEmpty(fileKey)) {
            String groupCode = getStringParameter(request,"groupCode");
            DynaBean group = metaResourceService.selectOneByNativeQuery("JE_CORE_EXCELGROUP", NativeQuery.build().applyWithParams(" GROUPCODE={0}", groupCode));
            if (group == null) {
                return BaseRespResult.errorResult("请执行保存后再上传文件!");
            }
            //导入数据
            JSONObject returnObj = excelService.impData(groupCode, fileKey, request);
            // 删除文件
            documentInternalRpcService.delFilesByKey(Lists.newArrayList(fileKey), SecurityUserHolder.getCurrentAccountRealUserId());
            if (returnObj.containsKey("code")) {
                return BaseRespResult.errorResult(returnObj.getString("msg"));
            } else {
                return BaseRespResult.successResult(returnObj.get("list"));
            }
        } else {
            return BaseRespResult.errorResult("文件上传失败!");
        }
    }

    /**
     * 数据导入窗口(修改文件重新上传)
     *
     * @param request
     */
    @RequestMapping(value = "/doUpdateData", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult doUpdateData(HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        excelService.doUpdate(dynaBean,request);
        return null;
    }

    /**
     *
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doPreviewData", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult doPreviewData(BaseMethodArgument param, HttpServletRequest request) {
        String groupCode = getStringParameter(request,"groupCode");
        String temGroupId = getStringParameter(request,"temGroupId");
        String temIds = getStringParameter(request,"temGroupId");
        Map<String,Object> map = excelService.impPreviewData(temIds,groupCode, temGroupId, request);
        //处理数据
        if (map.containsKey("error")) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.save.error"));
        } else {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.save.success"));
        }
    }
}
