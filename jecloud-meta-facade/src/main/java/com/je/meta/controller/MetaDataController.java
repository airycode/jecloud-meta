/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.google.common.base.Strings;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.service.dbswitch.model.response.MetadataTableDetailResponse;
import com.je.meta.service.dbswitch.model.response.SchemaTableDataResponse;
import com.je.meta.service.dbswitch.service.MetaDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(tags = {"元数据查询接口"})
@RestController
@RequestMapping(value = "/je/meta/resourceTable/metadata")

public class MetaDataController extends AbstractPlatformController {

    @Autowired
    private MetaDataService metaDataService;

    @ApiOperation(value = "模式列表")
    @PostMapping(value = "/schemas", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseRespResult allSchemas() {
        return BaseRespResult.successResult(metaDataService.allSchemas());
    }

    @ApiOperation(value = "物理表/视图表列表")
    @PostMapping(value = "/tables", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseRespResult allTables(HttpServletRequest request) {
        String schema = getStringParameter(request, "schema");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        String page = getStringParameter(request, "page");
        String limit = getStringParameter(request, "limit");
        //模糊查询
        String fuzzyValue = getStringParameter(request, "fuzzyValue");
        int intPage = 1;
        int intLimit = 30;
        if (page != null && limit != null) {
            intPage = Integer.parseInt(page);
            intLimit = Integer.parseInt(limit);
        }
        Page resultPage = metaDataService.allTables(schema, productId, fuzzyValue, intPage, intLimit);
        return BaseRespResult.successResultPage(resultPage.getRecords(), (long) resultPage.getTotal());

    }

    @ApiOperation(value = "导入")
    @PostMapping(value = "/import/tables", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseRespResult importTable(HttpServletRequest request) {
        String schema = getStringParameter(request, "schema");
        String tables = getStringParameter(request, "tables");
        String syParent = getStringParameter(request, "SY_PARENT");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        if (Strings.isNullOrEmpty(tables)) {
            return BaseRespResult.errorResult("导入的表不能为空！");
        }
        return BaseRespResult.successResult(metaDataService.importTable(schema, tables, syParent, productId));
    }

    @ApiOperation(value = "物理表/视图表信息")
    @PostMapping(value = "/meta/table", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseRespResult<MetadataTableDetailResponse> tableMeta(HttpServletRequest request) {
        String schema = getStringParameter(request, "schema");
        String table = this.getStringParameter(request, "table");
        return BaseRespResult.successResult(metaDataService.tableDetail(schema, table));
    }

    @ApiOperation(value = "物理表/视图表的数据内容")
    @PostMapping(value = "/data/table", produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseRespResult<SchemaTableDataResponse> tableData(HttpServletRequest request) {
        String table = this.getStringParameter(request, "table");
        String schema = getStringParameter(request, "schema");
        return BaseRespResult.successResult(metaDataService.tableData(schema, table));
    }

}
