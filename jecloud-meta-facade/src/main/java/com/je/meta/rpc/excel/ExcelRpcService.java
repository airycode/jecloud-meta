/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.excel;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import java.util.List;
import java.util.Map;

/**
 * Excel远程调用服务实现
 */
public interface ExcelRpcService {

    /**
     * 格式化数据
     */
    List<List<Object>> buildExportData(String funcId, List<DynaBean> datas, Map<String, Map<String, String>> ddInfos, List<DynaBean> formColumns);

    /**
     * 构建字典信息
     * @param columns
     * @return
     */
    Map<String, Map<String, String>> buildDicDatas(List<DynaBean> columns);

    /**
     * 获取列信息
     * @param funcId
     * @return
     */
    List<DynaBean> getReportColumns(String funcId ,String fieldPlan);


    /**
     * 获取Excel导出列头信息
     * @param funcId
     * @return
     */
    List<List<String>> getReportTile(String funcId,String title);

    /**
     * 获取功能对应的Excel模板
     *
     * @param funcId 功能ID
     * @return List<Map>
     */
    List<Map<String,Object>> funcTemplateList(String funcId);

    /**
     * 获取功能对应的Excel模板
     *
     * @param funcId 功能ID
     * @return List<Map>
     */
    List<Map<String,Object>> funcTemplateListWithComustomer(String funcId,String comustomer);

    /**
     * 构建SHEET信息
     *
     * @param sheetName
     * @param sheetOrder
     * @param fields
     * @param fieldConfigs
     * @return
     */
    JSONObject buildSheetInfoWithSheet(String previewTemId, String sheetName, Integer sheetOrder, List<DynaBean> fields, List<String> fieldConfigs);

    /**
     * 构建Sheet的信息
     *
     * @param groupIdm   分组ID
     * @param groupTemId TODO未处理
     * @return
     */
    JSONArray buildSheetInfo(String groupIdm, String groupTemId);

    /**
     * 解析指定字段的全局带值操作
     *
     * @param field       TODO未处理
     * @param paramValues TODO未处理
     * @param dzValues    TODO未处理
     * @param dzFields    TODO未处理
     * @param requestParams    请求参数
     */
    void parseAllDzValues(DynaBean field, Map<String,Object> paramValues, Map<String, List<DynaBean>> dzValues, List<String> dzFields, Map<String, String> requestParams);

    /**
     * 设定带值的值
     *
     * @param fieldInfo   TODO未处理
     * @param dynaBean    TODO未处理
     * @param dzValues    TODO未处理
     * @param paramValues TODO未处理
     */
    void doSetBeanDzValue(DynaBean fieldInfo, DynaBean dynaBean, Map<String, List<DynaBean>> dzValues, Map<String,Object> paramValues);

    /**
     * 元数据预览数据准备操作
     * @param code
     * @param temIds
     * @return
     */
    JSONObject preparePreviewData(String code, String temIds);

    /**
     *
     * @param excelGroupId
     */
    void afterPreviewData(String excelGroupId);


    List<Map<String, Object>> funcTemplateListById(String sheetId);
}
