/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import com.je.ibatis.extension.conditions.ConditionsWrapper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 资源表字段服务，此服务包括对外部模块的所有资源表字段服务
 */
public interface MetaTableColumnRpcService {


    /**
     * 检查字段是否存在某个视图
     */
    String checkColumnBelongtoView(String resourceId,String columnCode);

    /**
     * 验证字段是否可删除
     * @param ids
     * @return
     */
    public String checkTypeById(String ids);
    /**
     * 删除列
     *
     * @param dynaBean 自定义动态类
     * @param ids      要删除的列id对象，逗号分隔
+     * @param isDll    是否执行DLL
     * @return
     */
    String removeColumnWithDll(DynaBean dynaBean, String ids);

    /**
     * 删除资源表时，如果有视图使用此字段，则也删除
     *
     * @param tableCode 资源表编码
     * @param columns   删除列集合
     */
    void deleteCascadeViewColumn(String tableCode, List<DynaBean> columns);

    /**
     * 增量导入
     *
     * @param dynaBean
     */
    void impNewCols(DynaBean dynaBean);

    /**
     * 对列进行检测。。 主要检测字段为空和字段重复
     *
     * @param columns 列信息
     * @param jeCore  是否系统
     * @return
     */
    String checkColumns(List<DynaBean> columns, Boolean jeCore);

    /**
     * 获取删除指定表的字段的DDL
     *
     * @param tableCode 暂不明确
     * @param columns   暂不明确
     */
    List<String> requireDeletePhysicalColumnDDL(String tableCode, List<DynaBean> columns);

    /**
     * 物理删除指定表的字段
     *
     * @param tableCode 资源表
     * @param columns   表列
     */
    void deleteColumnMeta(String tableCode, List<DynaBean> columns);

    /**
     * 物理删除指定表的字段
     *
     * @param tableCode 暂不明确
     * @param columns   暂不明确
     * @param isDdl     暂不明确
     */
    void deleteColumn(String tableCode, List<DynaBean> columns, Boolean isDdl);

    String addColumnByDD( DynaBean dynaBean);

    String addColumnByDDList(DynaBean dynaBean);

    String addColumnByTable(DynaBean dynaBean);

    Integer addColumnByAtom(String strData, String tableCode, String pkValue);

    void addAtomByColumn(String strData, String atomcolumn_atom_id);

    void initCreateColumns(DynaBean table) throws APIWarnException;

    void initUpdateColumns(DynaBean table) throws APIWarnException;

    void initShColumns(DynaBean table);

    boolean addSystemColumn(HttpServletRequest request) throws APIWarnException;

    int doUpdateList(DynaBean dynaBean, String strData);

    String checkUnique(String ids, String pkValue);

    int doUpdateListByColumn(DynaBean dynaBean, String strData) throws APIWarnException;

    List<DynaBean> selectTableByPks(String tableCode, String[] split);

    Map<String, Object> addFieldToLogicTable(DynaBean dynaBean);

    String addField(DynaBean dynaBean);

    void initColumns(DynaBean resourceTable, Boolean isTree);

    String deleteIndex(String tableCode, List<DynaBean> indexs);

    /**
     * 根据列的id判断该列是否参与构建外键关系
     * @param ids
     * @return
     */
    boolean checkIsExistForeignKeyByColumnIds(String ids);
}
