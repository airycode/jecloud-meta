/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.dictionary;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.mapper.query.Query;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.dd.DictionaryItemVo;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class DictionaryRpcServiceImpl implements DictionaryRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "dictionaryRpcService")
    private DictionaryRpcService dictionaryRpcService;
    @Autowired
    private DictionaryItemRpcService dictionaryItemRpcService;

    public List<DynaBean> getAllDicItems(String ddCode){
        return dictionaryRpcService.getAllDicItems(ddCode);
    }

    @Override
    public List<DictionaryItemVo> buildChildrenList(DynaBean dic, Boolean en, Query query, String itemCode) {
        return dictionaryRpcService.buildChildrenList(dic,en,query,itemCode);
    }

    @Override
    public JSONObject getAllListDicItem() {
        return dictionaryRpcService.getAllListDicItem();
    }

    @Override
    public JSONObject getAllListDicItemWithWhere(String whereSql, Object... params) {
        return dictionaryRpcService.getAllListDicItemWithWhere(whereSql,params);
    }

    @Override
    public void doCacheAllListTypeDicItem() {
        dictionaryRpcService.doCacheAllListTypeDicItem();
    }

    @Override
    public void doCacheAllDicInfo() {
        dictionaryRpcService.doCacheAllDicInfo();
    }

    @Override
    public void reCacheDicInfo(String dicCodes) {
        dictionaryRpcService.reCacheDicInfo(dicCodes);
    }

    @Override
    public List<DictionaryItemVo> getDicList(String ddCode, Query query, Boolean en) {
        return dictionaryRpcService.getDicList(ddCode,query,en);
    }

    @Override
    public String getItemNameByCode(List<DictionaryItemVo> dicItems, String itemCode) {
        return dictionaryRpcService.getItemNameByCode(dicItems,itemCode);
    }

    @Override
    public List<JSONTreeNode> getAllTyepDdListItems(String ddCode, Map<String, String> params, QueryInfo queryInfo, Boolean en) {
        return dictionaryItemRpcService.getDdItems(ddCode,params,queryInfo,en);
    }

    @Override
    public void syncSsxDic() {
        dictionaryRpcService.syncSsxDic();
    }

}
