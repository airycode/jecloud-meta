/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:24
 * @description:
 */
@Service
public class MetaTableIndexRpcServiceImpl implements MetaTableIndexRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "metaTableIndexRpcService")
    private MetaTableIndexRpcService metaTableIndexRpcService;

    @Autowired
    private MetaService metaService;

    @Autowired
    private MetaResourceService metaRpcService;

    @Override
    public DynaBean saveLogicData(DynaBean dynaBean) {
        return metaTableIndexRpcService.saveLogicData(dynaBean);
    }

    @Override
    public String getDDL4AddIndex(DynaBean dynaBean) {
        return metaTableIndexRpcService.getDDL4AddIndex(dynaBean);
    }

    @Override
    public void initIndexs(DynaBean resourceTable, Boolean isTree) {
        metaTableIndexRpcService.initIndexs(resourceTable,isTree);
    }



    @Override
    public String removeIndexByColumn(String funcId, String columnCode, String columnId) {
        return metaTableIndexRpcService.removeIndexByColumn(funcId,columnCode,columnId);
    }

    @Override
    public String checkIndexs(List<DynaBean> indexs) {
        return metaTableIndexRpcService.checkIndexs(indexs);
    }

    @Override
    public Integer removeIndex(DynaBean dynaBean, String ids) {
        return metaTableIndexRpcService.removeIndex(dynaBean,ids);
    }

    @Override
    public void deleteIndex(String tableCode, List<DynaBean> indexs) {
        metaTableIndexRpcService.deleteIndex(tableCode,indexs);
    }

    @Override
    public String deleteIndexMeta(DynaBean dynaBean) {
        return metaTableIndexRpcService.deleteIndexMeta(dynaBean);
    }

    @Override
    public String requireDeletePhysicalIndexDDL(DynaBean dynaBean) {
        return metaTableIndexRpcService.requireDeletePhysicalIndexDDL(dynaBean);
    }

    @Override
    public String getDelIndexDdl(String ids) {
        if(checkIndexIsSys(ids)){
            return  MessageUtils.getMessage("index.delete.sys");
        }
        String delSql =metaTableIndexRpcService.getDelIndexDdl(ids);
        if (StringUtil.isNotEmpty(delSql)) {
            try {
                delSql = delSql.replaceAll("\n", "");
                String[] delSqlArray = delSql.split(";");
                for (String sql : delSqlArray) {
                    metaService.executeSql(sql);
                }
            } catch (UncategorizedSQLException e) {
                throw new PlatformException(MessageUtils.getMessage("index.delete.index.error") + e.getCause().getMessage(),
                        PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{delSql}, e);
            } catch (Exception e) {
                throw new PlatformException(MessageUtils.getMessage("index.delete.wrong") + "," + e.getCause().getMessage(),
                        PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{delSql}, e);
            }
        }
        return null;
    }

    private boolean checkIndexIsSys(String ids) {
      List<DynaBean> list =  metaRpcService.selectByNativeQuery(NativeQuery.build()
              .tableCode("JE_CORE_TABLEINDEX")
              .in("JE_CORE_TABLEINDEX_ID",ids)
              .eq("TABLEINDEX_CLASSIFY","SYS"));
      if(list!=null&&list.size()>0){
          return true;
      }
      return false;

    }


}
