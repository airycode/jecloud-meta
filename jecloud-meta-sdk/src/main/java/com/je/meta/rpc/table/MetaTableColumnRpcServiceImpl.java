/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:23
 * @description:
 */
@Service
public class MetaTableColumnRpcServiceImpl implements MetaTableColumnRpcService {
    private static final Logger logger = LoggerFactory.getLogger(MetaTableColumnRpcServiceImpl.class);

    @RpcReference(microserviceName = "meta", schemaId = "metaTableColumnRpcService")
    private MetaTableColumnRpcService metaTableColumnRpcService;

    @Autowired
    private MetaResourceService  metaResourceService;

    @Autowired
    private MetaService metaService;

    @Autowired
    private MetaTableRpcService metaTableRpcService;


    @Override
    public String checkColumnBelongtoView(String resourceId, String columnCode) {
        return metaTableColumnRpcService.checkColumnBelongtoView(resourceId,columnCode);
    }

    @Override
    public String checkTypeById(String ids){
        return  metaTableColumnRpcService.checkTypeById(ids);
    }

    @Override
    public String removeColumnWithDll(DynaBean dynaBean, String ids) {
        String ddlSql = metaTableColumnRpcService.removeColumnWithDll(dynaBean, ids);
        if (!Strings.isNullOrEmpty(ddlSql)) {
            for (String sql : ddlSql.split(";")) {
                if (!Strings.isNullOrEmpty(sql)) {
                    metaService.executeSql(sql);
                }
            }
        }
        List<DynaBean>  funcinfoList = metaResourceService.selectByTableCodeAndNativeQuery("JE_CORE_FUNCINFO", NativeQuery.build().eq("FUNCINFO_TABLENAME",dynaBean.getStr("TABLECOLUMN_TABLECODE")));
        for(DynaBean funcinfo:funcinfoList){
            metaService.clearMyBatisFuncCache(funcinfo.getStr("FUNCINFO_FUNCCODE"));
        }
        return String.valueOf(ids.split(",").length);
    }

    @Override
    public void deleteCascadeViewColumn(String tableCode, List<DynaBean> columns) {
        metaTableColumnRpcService.deleteCascadeViewColumn(tableCode, columns);
    }

    @Override
    public void impNewCols(DynaBean dynaBean) {
        metaTableColumnRpcService.impNewCols(dynaBean);
    }

    @Override
    public String checkColumns(List<DynaBean> columns, Boolean jeCore) {
        return metaTableColumnRpcService.checkColumns(columns, jeCore);
    }

    @Override
    public List<String> requireDeletePhysicalColumnDDL(String tableCode, List<DynaBean> columns) {
        return metaTableColumnRpcService.requireDeletePhysicalColumnDDL(tableCode, columns);
    }

    @Override
    public void deleteColumnMeta(String tableCode, List<DynaBean> columns) {
        metaTableColumnRpcService.deleteColumnMeta(tableCode, columns);
    }

    @Override
    public void deleteColumn(String tableCode, List<DynaBean> columns, Boolean isDdl) {
        metaTableColumnRpcService.deleteColumn(tableCode, columns, isDdl);
    }

    @Override
    public String addColumnByDD(DynaBean dynaBean) {
        return metaTableColumnRpcService.addColumnByDD(dynaBean);
    }

    @Override
    public String addColumnByDDList(DynaBean dynaBean) {
        return metaTableColumnRpcService.addColumnByDDList(dynaBean);
    }

    @Override
    public String addColumnByTable(DynaBean dynaBean) {
        return metaTableColumnRpcService.addColumnByTable(dynaBean);
    }

    @Override
    public Integer addColumnByAtom(String strData, String tableCode, String pkValue) {
        return metaTableColumnRpcService.addColumnByAtom(strData, tableCode, pkValue);
    }

    @Override
    public void addAtomByColumn(String strData, String atomcolumn_atom_id) {
        metaTableColumnRpcService.addAtomByColumn(strData, atomcolumn_atom_id);
    }

    @Override
    public void initCreateColumns(DynaBean table) throws APIWarnException {
        metaTableColumnRpcService.initCreateColumns(table);
    }

    @Override
    public void initUpdateColumns(DynaBean table) throws APIWarnException {
        metaTableColumnRpcService.initUpdateColumns(table);
    }

    @Override
    public void initShColumns(DynaBean table) {
        metaTableColumnRpcService.initShColumns(table);
    }

    @Override
    public boolean addSystemColumn(HttpServletRequest request) throws APIWarnException {
        return metaTableColumnRpcService.addSystemColumn(request);
    }

    @Override
    public int doUpdateList(DynaBean dynaBean, String strData) {
        return metaTableColumnRpcService.doUpdateList(dynaBean, strData);
    }

    @Override
    public String checkUnique(String ids, String pkValue) {
        return metaTableColumnRpcService.checkUnique(ids, pkValue);
    }

    @Override
    public int doUpdateListByColumn(DynaBean dynaBean, String strData) throws APIWarnException {
        return metaTableColumnRpcService.doUpdateListByColumn(dynaBean, strData);
    }

    @Override
    public List<DynaBean> selectTableByPks(String tableCode, String[] split) {
        return metaTableColumnRpcService.selectTableByPks(tableCode, split);
    }

    @Override
    public Map<String, Object> addFieldToLogicTable(DynaBean dynaBean) {
        return metaTableColumnRpcService.addFieldToLogicTable(dynaBean);
    }

    @Override
    public String addField(DynaBean dynaBean) {
        Map<String, Object> addResult = this.addFieldToLogicTable(dynaBean);
        if(addResult.containsKey("result")){
            if(StringUtil.isNotEmpty(addResult.get("result"))){
                return addResult.get("result").toString();
            }
        }
        metaTableRpcService.updateTable(addResult.get("resourcetableId") == null ? "" : addResult.get("resourcetableId").toString(), true);
        return null;
    }

    @Override
    public String deleteIndex(String tableCode, List<DynaBean> indexs) {
        return null;
    }

    @Override
    public boolean checkIsExistForeignKeyByColumnIds(String ids) {
        return metaTableColumnRpcService.checkIsExistForeignKeyByColumnIds(ids);
    }

    @Override
    public void initColumns(DynaBean resourceTable, Boolean isTree) {
        metaTableColumnRpcService.initColumns(resourceTable, isTree);
    }

}
