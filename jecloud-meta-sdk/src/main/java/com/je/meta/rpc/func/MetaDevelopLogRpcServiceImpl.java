/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.func;

import com.je.meta.rpc.develop.DevelopLogRpcService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

@Service
public class MetaDevelopLogRpcServiceImpl implements DevelopLogRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "metaDevelopLogRpcService")
    private DevelopLogRpcService developLogRpcService;

    @Override
    public long getMenuNum(String menuCode) {
        return developLogRpcService.getMenuNum(menuCode);
    }

    @Override
    public void doDevelopLog(String act, String actName, String type, String typeName, String name, String code, String id,String productId) {
        developLogRpcService.doDevelopLog(act,actName,type,typeName,name,code,id,productId);
    }

}
