/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.CommonTableRpcService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ChangePrimaryKeyRpcServiceImpl implements ChangePrimaryKeyRpcService {

    @RpcReference(microserviceName = "meta", schemaId = "changePrimaryKeyRpcService")
    private ChangePrimaryKeyRpcService changePrimaryKeyRpcService;

    @Autowired
    private MetaService metaService;

    @Override
    public String checkLocal(DynaBean table) {
        String result="";
        if (StringUtil.isEmpty(result) && !"VIEW".equals(table.getStr("RESOURCETABLE_TYPE"))) {
            result = checkIsExistsData(table.getStr("RESOURCETABLE_TABLECODE"), table.getStr("SY_PRODUCT_CODE"));
        }
        return result;
    }

    @Override
    public String checkMeta(String newPrimaryKey, DynaBean tableInfo) {
        return changePrimaryKeyRpcService.checkMeta(newPrimaryKey,tableInfo);
    }


    @Override
    public String checkIsExistsData(String tableCode, String productCode) {
        long count;
        count = getDataCount(tableCode);
        if(count>0){
            return MessageUtils.getMessage("table.exist.data");
        }
        return null;
    }

    private long getDataCount(String tableCode) {
        List<Map<String,Object>> list = metaService.selectSql(ConditionsWrapper.builder()
                .apply(StringUtil.isNotEmpty(tableCode),"Select count(1) as count from "+ tableCode ));
        if(list!=null){
            return list.get(0).get("count")==null?0L:Long.parseLong(list.get(0).get("count").toString());
        }
        return 0L;
    }

    @Override
    public boolean checkNewPrimaryKeyIsExistsOfCurrentTable(String tableCode, String newPrimaryKey) {
        return changePrimaryKeyRpcService.checkNewPrimaryKeyIsExistsOfCurrentTable(tableCode,newPrimaryKey);
    }

    @Override
    public String checkPrimaryKeyBelongToView(String pkValue, String pkCode) {
        return null;
    }

    @Override
    public void updateMeta(DynaBean tableInfo, String newPrimaryKey) {
        changePrimaryKeyRpcService.updateMeta(tableInfo,newPrimaryKey);
    }
}
