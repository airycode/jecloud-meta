/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.excel;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @description: Excel远程调用实现
 */
@Service
public class ExcelRpcServiceImpl implements ExcelRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "excelRpcService" )
    private ExcelRpcService excelRpcService;

    @Override
    public List<List<Object>> buildExportData(String funcId, List<DynaBean> datas, Map<String, Map<String, String>> ddInfos, List<DynaBean> formColumns) {
        return excelRpcService.buildExportData(funcId,datas,ddInfos,formColumns);
    }

    @Override
    public Map<String, Map<String, String>> buildDicDatas(List<DynaBean> columns) {
        return excelRpcService.buildDicDatas(columns);
    }

    @Override
    public List<DynaBean> getReportColumns(String funcId,String fieldPlan) {
        return excelRpcService.getReportColumns(funcId,fieldPlan);
    }

    @Override
    public List<List<String>> getReportTile(String funcId ,String title) {
        return excelRpcService.getReportTile(funcId,title);
    }

    @Override
    public JSONObject preparePreviewData(String code, String temIds) {
        return excelRpcService.preparePreviewData(code,temIds);
    }

    @Override
    public void afterPreviewData(String excelGroupId) {
        excelRpcService.afterPreviewData(excelGroupId);
    }

    @Override
    public List<Map<String, Object>> funcTemplateListById(String sheetId) {
        return excelRpcService.funcTemplateListById(sheetId);
    }

    @Override
    public List<Map<String, Object>> funcTemplateList(String funcId) {
        return excelRpcService.funcTemplateList(funcId);
    }

    @Override
    public List<Map<String, Object>> funcTemplateListWithComustomer(String funcId, String comustomer) {
        return excelRpcService.funcTemplateListWithComustomer(funcId,comustomer);
    }

    @Override
    public JSONObject buildSheetInfoWithSheet(String previewTemId, String sheetName, Integer sheetOrder, List<DynaBean> fields, List<String> fieldConfigs) {
        return excelRpcService.buildSheetInfoWithSheet(previewTemId,sheetName,sheetOrder,fields,fieldConfigs);
    }

    @Override
    public JSONArray buildSheetInfo(String groupId, String groupTemId) {
        return excelRpcService.buildSheetInfo(groupId,groupTemId);
    }

    @Override
    public void parseAllDzValues(DynaBean field, Map<String, Object> paramValues, Map<String, List<DynaBean>> dzValues, List<String> dzFields, Map<String, String> requestParams) {
        excelRpcService.parseAllDzValues(field,paramValues,dzValues,dzFields,requestParams);
    }

    @Override
    public void doSetBeanDzValue(DynaBean fieldInfo, DynaBean dynaBean, Map<String, List<DynaBean>> dzValues, Map<String, Object> paramValues) {
        excelRpcService.doSetBeanDzValue(fieldInfo,dynaBean,dzValues,paramValues);
    }

}
