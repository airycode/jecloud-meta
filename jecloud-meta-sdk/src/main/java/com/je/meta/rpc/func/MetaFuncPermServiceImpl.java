/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.func;

import com.je.common.base.func.funcPerm.ControlFieldAuthVo;
import com.je.common.base.func.funcPerm.FieldAuth.FieldAuthVo;
import com.je.common.base.func.funcPerm.dictionaryAuth.DictionaryAuthVo;
import com.je.common.base.func.funcPerm.fastAuth.FastAuthVo;
import com.je.common.base.func.funcPerm.fastAuth.FuncQuickPermission;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

@Service
public class MetaFuncPermServiceImpl implements MetaFuncPermService {

    @RpcReference(microserviceName = "meta",schemaId = "metaFuncPermService")
    private MetaFuncPermService metaFuncPermService;

    @Override
    public FastAuthVo getFastAuth(String funcCode) {
        return metaFuncPermService.getFastAuth(funcCode);
    }

    @Override
    public FieldAuthVo getFiledAuth(String funcCode) {
        return metaFuncPermService.getFiledAuth(funcCode);
    }

    @Override
    public DictionaryAuthVo getDictionaryAuth(String funcCode) {
        return metaFuncPermService.getDictionaryAuth(funcCode);
    }

    @Override
    public RoleSqlAuthVo getRoleSqlAuth(String funcCode) {
        return metaFuncPermService.getRoleSqlAuth(funcCode);
    }

    @Override
    public String getSqlAuth(String funcCode) {
        return metaFuncPermService.getSqlAuth(funcCode);
    }

    @Override
    public ControlFieldAuthVo controlField(String funcCode) {
        return metaFuncPermService.controlField(funcCode);
    }

    @Override
    public FuncQuickPermission getFuncQuickPermission(String funcCode) {
        return metaFuncPermService.getFuncQuickPermission(funcCode);
    }

    @Override
    public void restoreDefault(String funcCode) {
        metaFuncPermService.restoreDefault(funcCode);
    }

    @Override
    public void putRedisCache(String funcCode, String type) {
        metaFuncPermService.putRedisCache(funcCode,type);
    }

}
