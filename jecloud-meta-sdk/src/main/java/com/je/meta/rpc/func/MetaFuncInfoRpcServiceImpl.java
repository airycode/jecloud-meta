/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.func;

import com.je.common.base.DynaBean;
import com.je.common.base.entity.func.FuncInfo;
import com.je.common.base.entity.func.FuncRelationField;
import com.je.common.base.service.rpc.FunInfoRpcService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class MetaFuncInfoRpcServiceImpl implements FunInfoRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "metaFuncInfoRpcService")
    private FunInfoRpcService funInfoRpcService;

    @Override
    public FuncInfo getFuncInfo(String funcCode) {
        return funInfoRpcService.getFuncInfo(funcCode);
    }

    @Override
    public Map<String, Object> getFuncConfigInfo(DynaBean funcInfo, boolean refresh) {
        return funInfoRpcService.getFuncConfigInfo(funcInfo,refresh);
    }

    @Override
    public void buildDefaultFuncInfo(DynaBean funcInfo) {
        funInfoRpcService.buildDefaultFuncInfo(funcInfo);
    }

    @Override
    public DynaBean updateFunInfo(DynaBean funcInfo) {
        return funInfoRpcService.updateFunInfo(funcInfo);
    }

    @Override
    public void removeFuncInfoById(String funcId) {
        funInfoRpcService.removeFuncInfoById(funcId);
    }

    @Override
    public String buildWhereSql4funcRelation(List<FuncRelationField> relatedFields, DynaBean dynaBean) {
        return funInfoRpcService.buildWhereSql4funcRelation(relatedFields,dynaBean);
    }

    @Override
    public DynaBean clearFuncInfo(DynaBean funcInfo) {
        return funInfoRpcService.clearFuncInfo(funcInfo);
    }

    @Override
    public void initFuncInfo(DynaBean funcInfo) {
        funInfoRpcService.initFuncInfo(funcInfo);
    }

    @Override
    public void removeFuncRelyon(String funcRelyonId) {
        funInfoRpcService.removeFuncRelyon(funcRelyonId);
    }

    @Override
    public void syncConfig(String funcId, String configFuncIds) {
        funInfoRpcService.syncConfig(funcId,configFuncIds);
    }
    
}
