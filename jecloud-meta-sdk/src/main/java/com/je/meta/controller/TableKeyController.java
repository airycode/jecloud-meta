/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.table.MetaTableKeyRpcService;
import com.je.meta.rpc.table.MetaTableRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 资源表-操作键
 */
@RestController
@RequestMapping("/je/meta/resourceTable/table/tableKey")
public class TableKeyController extends AbstractPlatformController {

    @Autowired
    private MetaTableKeyRpcService metaTableKeyRpcService;
    @Autowired
    private MetaTableRpcService metaTableRpcService;

    /**
     * 删除键
     *
     * @param param
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/doDelete", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult removeKey(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String ids = getStringParameter(request, "tableKeyIds");
        String isDeleteDdl = getStringParameter(request, "deleteDdl");
        if (StringUtil.isEmpty(ids)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("key.id.canNotEmpty"));
        }
        try {
            String[] split = ids.split(ArrayUtils.SPLIT);
            DynaBean table = metaTableRpcService.selectOneByPk("JE_CORE_TABLEKEY", split[0]);
            List<DynaBean> keys = metaTableKeyRpcService.selectTableKeyByKeyId(ids);
            metaTableKeyRpcService.doRemoveKey(dynaBean, keys, isDeleteDdl, table.getStr("TABLEKEY_TABLECODE"));
            Integer count = metaTableKeyRpcService.removeKey(dynaBean, ids, "0");
            return BaseRespResult.successResult(MessageUtils.getMessage("common.delete.success"));
        } catch (Exception e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.delete.error"));
        }
    }

}
