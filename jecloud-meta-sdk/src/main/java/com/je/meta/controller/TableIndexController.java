/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaRpcService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.table.MetaTableIndexRpcService;
import com.je.meta.rpc.table.MetaTableKeyRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 资源表-操作键
 */
@RestController
@RequestMapping("/je/meta/resourceTable/table/tableIndex")
public class TableIndexController extends AbstractPlatformController {

    @Autowired
    private MetaTableIndexRpcService metaTableIndexRpcService;



    /**
     * 按照列删除索引-功能列表删除索引
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/removeIndexByColumn", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult removeIndexByColumn(BaseMethodArgument param,HttpServletRequest request) {
        String funcId = param.getFuncId();
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String columnId = getStringParameter(request,"columnId");
        dynaBean.setStr("funcId",funcId);
        dynaBean.setStr("columnId",columnId);
        String ddl = metaTableIndexRpcService.requireDeletePhysicalIndexDDL(dynaBean);
        metaService.executeSql(ddl);
        String result =  metaTableIndexRpcService.deleteIndexMeta(dynaBean);
       if(StringUtil.isNotEmpty(result)){
           return BaseRespResult.errorResult(result);
       }
        return BaseRespResult.successResult(MessageUtils.getMessage("common.delete.success"));
    }

    /**
     * 按照列创建索引-功能列表添加索引
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/createIndexByColumn", method = RequestMethod.POST)
    public BaseRespResult createIndexByColumn(BaseMethodArgument param,HttpServletRequest request) {
        List<DynaBean> list = new ArrayList<>();
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String funcId = param.getFuncId();
        String columnId = getStringParameter(request, "columnId");
        dynaBean.setStr("funcId",funcId);
        dynaBean.setStr("columnId",columnId);
        list.add(dynaBean);
        String result = metaTableIndexRpcService.checkIndexs(list);
        if(StringUtil.isNotEmpty(result)){
            return BaseRespResult.errorResult(result);
        }
        DynaBean data = metaTableIndexRpcService.saveLogicData(dynaBean);
        if(result == null){
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.create.error"));
        }
        String ddl = metaTableIndexRpcService.getDDL4AddIndex(data);
        metaService.executeSql(ddl);
        return BaseRespResult.successResult(data);
    }
    /**
     * 删除键
     *
     * @param param
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/doDelete", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult removeIndex(BaseMethodArgument param, HttpServletRequest request) {
        String ids = getStringParameter(request, "tableIndexIds");
        if (StringUtil.isEmpty(ids)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("index.id.canNotEmpty"));
        }

        DynaBean dynaBean = new DynaBean();
        dynaBean.setStr("tableIndexIds",ids);
        /**
         * 删除物理表索引
         */
        String result = metaTableIndexRpcService.getDelIndexDdl(ids);
        if(StringUtil.isNotEmpty(result)){
            return BaseRespResult.errorResult(result);
        }
        /**
         * 删除逻辑表索引
         */
        metaTableIndexRpcService.deleteIndexMeta(dynaBean);
        return BaseRespResult.successResult(MessageUtils.getMessage("common.delete.success"));
    }

}
