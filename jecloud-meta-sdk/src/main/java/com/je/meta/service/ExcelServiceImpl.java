/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.meta.model.ExcelParamVo;
import com.je.meta.model.ExcelReturnVo;
import com.je.meta.rpc.excel.ExcelRpcExportService;
import com.je.meta.rpc.excel.ExcelRpcService;
import com.je.meta.service.excel.ExcelExportFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class ExcelServiceImpl extends AbstractExcelServiceImpl implements ExcelService {
    @Autowired
   private ExcelRpcService excelRpcService;

    @Override
    public List<List<Object>> buildExportData(String funcId, List<DynaBean> datas, Map<String, Map<String, String>> ddInfos, List<DynaBean> formColumns) {
        return excelRpcService.buildExportData(funcId,datas,ddInfos,formColumns);
    }

    @Override
    public InputStream exportExcel(JSONObject params, HttpServletRequest request) {
        String styleType = params.getString("styleType");
        ExcelRpcExportService excelRpcExportService = ExcelExportFactory.getValidator(styleType);
        return excelRpcExportService.export(params, request);
    }

    @Override
    public JSONObject impData(DynaBean groupTemBean, String fileKey, HttpServletRequest request) {

        return null;
    }

    @Override
    public JSONObject impData(String code, String fileKey, HttpServletRequest request) {
        return null;
    }

    @Override
    public JSONObject impData(String code, String filePath, String groupTemId, HttpServletRequest request) {
        return null;
    }

    @Override
    public void impPreviewData(String code, String temIds, HttpServletRequest request, JSONObject returnObj) {

    }

    @Override
    public ExcelReturnVo executeLogic(String type, String sql, String datasourceName, String procedureName, String pkValue, String queryType, String beanName, String beanMethod, Map<String, Object> paramValues, ExcelParamVo paramVo) {
        return null;
    }

    @Override
    public void implExcelFields(DynaBean dynaBean) {

    }

    @Override
    public List<Map<String, Object>> funcTemplateListById(String sheetId) {
        return null;
    }

}
