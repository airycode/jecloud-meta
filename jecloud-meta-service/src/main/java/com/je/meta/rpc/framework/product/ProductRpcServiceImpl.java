/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.framework.product;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

@RpcSchema(schemaId = "productRpcService")
public class ProductRpcServiceImpl implements ProductRpcService {

    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean getInfoById(String productId) {
        return metaService.selectOneByPk("JE_PRODUCT_MANAGE", productId);
    }

    @Override
    public void buildDynaBeanProductInfo(DynaBean dynaBean, String productId) {
        DynaBean productTable = getInfoById(productId);
        if (productId == null) {
            throw new PlatformException("产品信息未找到！", PlatformExceptionEnum.UNKOWN_ERROR);
        }
        dynaBean.set("SY_PRODUCT_ID", productId);
        dynaBean.set("SY_PRODUCT_NAME", productTable.getStr("PRODUCT_NAME"));
        dynaBean.set("SY_PRODUCT_CODE", productTable.getStr("PRODUCT_CODE"));
    }
}
