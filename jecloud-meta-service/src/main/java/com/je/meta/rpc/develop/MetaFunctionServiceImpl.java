/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.develop;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.FunctionService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import java.util.List;

@RpcSchema(schemaId = "metaFunctionService")
public class MetaFunctionServiceImpl implements FunctionService {

    protected static final String TABLES = "select JE_CORE_RESOURCETABLE_ID,RESOURCETABLE_TABLENAME,RESOURCETABLE_TABLECODE,RESOURCETABLE_PKCODE from je_core_resourcetable where SY_DISABLED='0'";
    protected static final String TABLES_IN = "select JE_CORE_RESOURCETABLE_ID,RESOURCETABLE_TABLENAME,RESOURCETABLE_TABLECODE,RESOURCETABLE_PKCODE from je_core_resourcetable where SY_DISABLED='0' AND RESOURCETABLE_TABLECODE =?";
    protected static final String KEYS = "select TABLEKEY_COLUMNCODE,TABLEKEY_TABLECODE from JE_CORE_TABLEKEY where TABLEKEY_RESOURCETABLE_ID=? and TABLEKEY_TYPE = 'Primary' AND TABLEKEY_ISCREATE = 1 order by SY_ORDERINDEX";
    protected static final String COLUMNS = "select TABLECOLUMN_NAME,TABLECOLUMN_CODE,TABLECOLUMN_TYPE,TABLECOLUMN_LENGTH from JE_CORE_TABLECOLUMN where TABLECOLUMN_ISCREATE = 1 AND TABLECOLUMN_RESOURCETABLE_ID=?";
    protected static final String FUNCTIONS = "select JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCNAME,FUNCINFO_FUNCCODE,FUNCINFO_TABLENAME,FUNCINFO_COLUMNLAZY from JE_CORE_FUNCINFO where FUNCINFO_NODEINFOTYPE IN ('FUNCFIELD','FUNC') order by SY_ORDERINDEX";
    protected static final String FUNCTIONS_IN = "select JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCNAME,FUNCINFO_FUNCCODE,FUNCINFO_TABLENAME,FUNCINFO_COLUMNLAZY from JE_CORE_FUNCINFO where FUNCINFO_NODEINFOTYPE IN ('FUNCFIELD','FUNC') AND FUNCINFO_FUNCCODE = ? order by SY_ORDERINDEX";
    protected static final String FUNCTION_LOAD_COLUMNS = "select RESOURCECOLUMN_CODE,RESOURCECOLUMN_NAME from JE_CORE_RESOURCECOLUMN where RESOURCECOLUMN_LAZYLOAD ='1' AND RESOURCECOLUMN_FUNCINFO_ID = ?";

    @Autowired
    private MetaService metaService;

    @Override
    @PostMapping(value = "/findAllFunctions",produces = "application/json")
    public List<DynaBean> findAllFunctions() {
        List<DynaBean> result = null;
        try {
            result = metaService.select("JE_CORE_FUNCINFO", ConditionsWrapper.builder()
                    .in("FUNCINFO_NODEINFOTYPE", Lists.newArrayList("FUNCFIELD","FUNC"))
                    .orderByAsc("SY_ORDERINDEX"),"JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCNAME,FUNCINFO_FUNCCODE,FUNCINFO_TABLENAME,FUNCINFO_COLUMNLAZY");
        }catch (Throwable e){
            e.printStackTrace();
        }

        return result;
    }

    @Override
    @PostMapping(value = "/findFunction",consumes = "application/json")
    public DynaBean findFunction(String funcCode) {
        DynaBean result = metaService.selectOne("JE_CORE_FUNCINFO",ConditionsWrapper.builder()
                .in("FUNCINFO_NODEINFOTYPE", Lists.newArrayList("FUNCFIELD","FUNC"))
                .eq("FUNCINFO_FUNCCODE",funcCode).orderByAsc("SY_ORDERINDEX"),"JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCNAME,FUNCINFO_FUNCCODE,FUNCINFO_TABLENAME,FUNCINFO_COLUMNLAZY");
        return result;
    }

    @Override
    @PostMapping(value = "/findFunctionById",consumes = "application/json")
    public DynaBean findFunctionById(String functionId) {
        DynaBean result = metaService.selectOneByPk("JE_CORE_FUNCINFO",functionId,"JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCNAME,FUNCINFO_FUNCCODE,FUNCINFO_TABLENAME,FUNCINFO_COLUMNLAZY");
        return result;
    }

    @Override
    @PostMapping(value = "/findResourceColumnByFunctionId",consumes = "application/json")
    public List<DynaBean> findResourceColumnByFunctionId(String functionId) {
        List<DynaBean> result = metaService.select("JE_CORE_RESOURCECOLUMN",ConditionsWrapper.builder()
                .eq("RESOURCECOLUMN_LAZYLOAD","1").eq("RESOURCECOLUMN_FUNCINFO_ID",functionId));
        return result;
    }
}
