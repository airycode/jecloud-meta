/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.table.service.TableIndexRpcService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "tableIndexRpcService")
public class TableIndexRpcServiceImpl implements TableIndexRpcService {
    @Autowired
    MetaService metaService;

    @Override
    public DynaBean getTableIndexInfo(String columnCode, String tableCode) {
        List<DynaBean> list = metaService.select("JE_CORE_TABLEINDEX", ConditionsWrapper.builder()
                .eq("TABLEINDEX_TABLECODE", tableCode)
                .eq("TABLEINDEX_FIELDCODE", columnCode)
                .eq("TABLEINDEX_ISCREATE", "1"));
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public DynaBean insertTableIndex(String columnCode, String tableCode, String indexName) {
        List<DynaBean> listColumn = metaService.select("JE_CORE_TABLECOLUMN", ConditionsWrapper.builder()
                .eq("TABLECOLUMN_TABLECODE", tableCode)
                .eq("TABLECOLUMN_CODE", columnCode));
        String TABLEINDEX_FIELDNAME="";
        if(listColumn!=null&&listColumn.size()>0){
            TABLEINDEX_FIELDNAME=listColumn.get(0).getStr("TABLECOLUMN_NAME");
        }
        List<DynaBean> list = metaService.select("JE_CORE_TABLEINDEX", ConditionsWrapper.builder()
                .eq("TABLEINDEX_TABLECODE", tableCode)
                .eq("TABLEINDEX_FIELDCODE", columnCode)
                .eq("TABLEINDEX_ISCREATE", "0"));
        if (list.size() > 0) {
            for (DynaBean index : list) {
                index.setStr("TABLEINDEX_ISCREATE", "1");
                metaService.update(index);
            }
            return list.get(0);
        }

        /**ID的键设置*/
        DynaBean index = new DynaBean("JE_CORE_TABLEINDEX", false);
        if (Strings.isNullOrEmpty(indexName)) {
            index.set("TABLEINDEX_NAME", "JE_" + DateUtils.getUniqueTime());
        } else {
            index.set("TABLEINDEX_NAME", indexName);
        }
        index.set("TABLEINDEX_FIELDCODE", columnCode);
        index.set("TABLEINDEX_FIELDNAME", TABLEINDEX_FIELDNAME);
        index.set("TABLEINDEX_ISCREATE", "1");
        index.set("TABLEINDEX_UNIQUE", "0");
        index.set("TABLEINDEX_CLASSIFY", "PRO");
        index.set("SY_ORDERINDEX", 99);

        String nowDate = DateUtils.formatDateTime(new Date());
        DynaBean table = metaService.selectOne("JE_CORE_RESOURCETABLE", ConditionsWrapper.builder().eq("RESOURCETABLE_TABLECODE", tableCode));
        index.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
        index.set("SY_CREATETIME", nowDate);
        index.set("SY_CREATEUSERID", SecurityUserHolder.getCurrentAccountRealUserId());
        index.set("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
        index.set("SY_CREATEORGID", SecurityUserHolder.getCurrentAccountRealOrgId());
        index.set("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
        index.set("TABLEINDEX_TABLECODE", tableCode);
        index.set("TABLEINDEX_RESOURCETABLE_ID", table.getPkValue());
        index.set("SY_PRODUCT_ID", table.get("SY_PRODUCT_ID"));
        index.set("SY_PRODUCT_NAME", table.getStr("SY_PRODUCT_NAME"));
        metaService.insert(index);
        return index;
    }

    @Override
    public List<Map<String, Object>> getIndexInfosToDatabase(String tableCode, String columnCode) {
        if(tableCode==null){
            return null;
        }
        return metaService.selectSql(String.format("SHOW INDEX FROM %s WHERE COLUMN_NAME='%s'", tableCode, columnCode));
    }

}
