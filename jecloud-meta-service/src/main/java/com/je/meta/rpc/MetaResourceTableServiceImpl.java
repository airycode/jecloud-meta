/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.table.ResourceTableService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@RpcSchema(schemaId = "metaResourceTableService")
public class MetaResourceTableServiceImpl implements ResourceTableService {

    @Autowired
    private MetaService metaService;

    @Override

    public List<DynaBean> findAllTables() {
        List<DynaBean> result = metaService.select("JE_CORE_RESOURCETABLE",
                ConditionsWrapper.builder().eq("SY_DISABLED","0"),
                "JE_CORE_RESOURCETABLE_ID,RESOURCETABLE_TABLENAME,RESOURCETABLE_TABLECODE,RESOURCETABLE_PKCODE");
        return result;
    }

    @Override
    public DynaBean findTable(String tableCode) {
        DynaBean result = metaService.selectOne("JE_CORE_RESOURCETABLE",ConditionsWrapper.builder()
                .eq("SY_DISABLED","0").eq("RESOURCETABLE_TABLECODE",tableCode));
        return result;
    }

    @Override
    public List<DynaBean> findTables(List<String> tableCodes) {
        List<DynaBean> result = metaService.select("JE_CORE_RESOURCETABLE",ConditionsWrapper.builder()
                .eq("SY_DISABLED","0").in("RESOURCETABLE_TABLECODE",tableCodes),"JE_CORE_RESOURCETABLE_ID,RESOURCETABLE_TABLENAME,RESOURCETABLE_TABLECODE,RESOURCETABLE_PKCODE");
        return result;
    }

    @Override
    public DynaBean findTableById(String resourceTableId) {
        DynaBean result = metaService.selectOneByPk("JE_CORE_RESOURCETABLE",resourceTableId,"JE_CORE_RESOURCETABLE_ID,RESOURCETABLE_TABLENAME,RESOURCETABLE_TABLECODE,RESOURCETABLE_PKCODE");
        return result;
    }

    @Override
    public List<DynaBean> findTableColumns(String resourceTableId) {
        List<DynaBean> result = metaService.select("JE_CORE_TABLECOLUMN",ConditionsWrapper.builder()
                .eq("TABLECOLUMN_ISCREATE","1").eq("TABLECOLUMN_RESOURCETABLE_ID",resourceTableId)
                ,"TABLECOLUMN_NAME,TABLECOLUMN_CODE,TABLECOLUMN_TYPE,TABLECOLUMN_LENGTH");
        return result;
    }

    @Override
    public List<DynaBean> findTableKeys(String resourceTableId) {
        List<DynaBean> result = metaService.select("JE_CORE_TABLEKEY",ConditionsWrapper.builder()
                .eq("TABLEKEY_RESOURCETABLE_ID",resourceTableId)
                .eq("TABLEKEY_TYPE","Primary")
                .eq("TABLEKEY_ISCREATE","1")
                .orderByAsc("SY_ORDERINDEX"));
        return result;
    }

}
