/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.product;


import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BaseProductRpcService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.product.PlatformProductCache;
import com.je.meta.service.setting.MetaSystemSettingService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "baseProductRpcService")
public class BaseProductRpcServicceImpl implements BaseProductRpcService {
    
    @Autowired
    private MetaService metaService;
    @Autowired
    private PlatformProductCache platformProductCache;

    @Override
    public List<String> findPlatformProductIds() {
        Map<String, DynaBean> values = platformProductCache.getCacheValues();
        if(values == null || values.isEmpty()){
            List<DynaBean> productBeanList = metaService.select("JE_PRODUCT_MANAGE", ConditionsWrapper.builder().eq("PRODUCT_TYPE","2"));
            for (DynaBean eachBean : productBeanList) {
                values.put(eachBean.getStr("PRODUCT_CODE"),eachBean);
            }
            platformProductCache.putMapCache(values);
        }
        
        List<String> idList = new ArrayList<>();
        for (DynaBean each : values.values()) {
            idList.add(each.getStr("JE_PRODUCT_MANAGE_ID"));
        }

        return idList;
    }

}
