/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.listener;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.setting.SystemDecryptCache;
import org.apache.servicecomb.core.BootListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemDescrptInitListener implements BootListener {

    private static final List<String> DESCRPT_KEY = new ArrayList<>();

    static {
        DESCRPT_KEY.add("JE_SYS_ENCRYPT");
        DESCRPT_KEY.add("JE_SYS_ENCRYPT_FIELD");
        DESCRPT_KEY.add("JE_SYS_ENCRYPT_KEY1");
        DESCRPT_KEY.add("JE_SYS_ENCRYPT_KEY2");
        DESCRPT_KEY.add("JE_SYS_ENCRYPT_KEY3");
    }

    @Override
    public int getOrder() {
        return 102;
    }

    @Override
    public void onAfterRegistry(BootEvent event) {
        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
        List<DynaBean> beanList = metaService.select("JE_CORE_SETTING", ConditionsWrapper.builder().in("CODE",DESCRPT_KEY));
        SystemDecryptCache systemDecryptCache = SpringContextHolder.getBean(SystemDecryptCache.class);
        Map<String,String> keyMap = new HashMap<>();
        for (DynaBean eachBean : beanList) {
            keyMap.put(eachBean.getStr("CODE"),eachBean.getStr("VALUE"));
        }
        systemDecryptCache.putMapCache(keyMap);
    }

}
