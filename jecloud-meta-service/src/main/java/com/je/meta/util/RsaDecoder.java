/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Charsets;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

/**
 * 加密
 */
public class RsaDecoder {

    /**
     * 对称加密算法
     */
    private static final String KEY_AES_ALGORITHM = "AES/CBC/PKCS7Padding";

    /**
     * 非对称加密算法
     */
    private static final String KEY_RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";

    /**
     *  Description: 解密接收数据
     *  @author  wh.huang  DateTime 2018年11月15日 下午5:06:42
     *  @param selfPrivateKey
     *  @param receiveData
     *  @throws InvalidKeyException
     *  @throws NoSuchPaddingException
     *  @throws NoSuchAlgorithmException
     *  @throws BadPaddingException
     *  @throws IllegalBlockSizeException
     *  @throws UnsupportedEncodingException
     *  @throws InvalidAlgorithmParameterException
     */
    public static String decryptReceivedData(PrivateKey selfPrivateKey, String receiveData) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        @SuppressWarnings("unchecked")
        Map<String, String> receivedMap = (Map<String, String>) JSON.parse(receiveData);

        // 解密请求方在发送请求时，加密data字段所用的对称加密密钥
        String a = receivedMap.get("a");
        String b = receivedMap.get("b");
        b = decryptRSA(selfPrivateKey, b);
        a = decryptRSA(selfPrivateKey, a);

        // 解密data数据
        String c = decryptAES(b, a, receivedMap.get("c"));
        return c;
    }

    /**
     * 根据私钥字符串生成私钥对象
     * @param base64PrivateKey
     * @return
     */
    public static PrivateKey generatePrivateKey(String base64PrivateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PKCS8EncodedKeySpec priPKCS8;
        priPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes(Charsets.UTF_8)));
        KeyFactory keyf = KeyFactory.getInstance("RSA");
        return keyf.generatePrivate(priPKCS8);
    }

    /**
     * 对称解密数据
     *
     * @param keyWithBase64
     * @param cipherText
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     */
    private static String decryptAES(String keyWithBase64, String ivWithBase64, String cipherText)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, InvalidAlgorithmParameterException {
        byte[] keyWithBase64Arry = keyWithBase64.getBytes();
        byte[] ivWithBase64Arry = ivWithBase64.getBytes();
        byte[] cipherTextArry = cipherText.getBytes();
        SecretKeySpec key = new SecretKeySpec(Base64.getDecoder().decode(keyWithBase64Arry), "AES");
        IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(ivWithBase64Arry));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        Cipher cipher = Cipher.getInstance(KEY_AES_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key, iv);
        return new String(cipher.doFinal(Base64.getDecoder().decode(cipherTextArry)), Charsets.UTF_8);
    }

    /**
     * 根据私钥和加密内容产生原始内容
     * @param key
     * @param content
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws UnsupportedEncodingException
     * @throws InvalidAlgorithmParameterException
     */
    private static String decryptRSA(Key key, String content) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance(KEY_RSA_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] contentArry = content.getBytes();
        return new String(cipher.doFinal(Base64.getDecoder().decode(contentArry)), Charsets.UTF_8);
    }

    /**
     * 钢戳验证
     * @param upgradeJson
     * @param license
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws UnsupportedEncodingException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     */
    public static boolean stampVerify(String upgradeJson,String license) throws InvalidKeySpecException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException {

        //解密信息
        JSONObject licenseObj = JSON.parseObject(license);
        String d = licenseObj.getString("d");
        PrivateKey nameKey = RsaDecoder.generatePrivateKey(d);
        String nameValue = RsaDecoder.decryptReceivedData(nameKey,license);
        JSONObject nameObj = JSON.parseObject(nameValue);

        //解密升级包中的信息
        JSONObject upgradeJsonObj = JSON.parseObject(upgradeJson);
        String fieldA = upgradeJsonObj.getString("UPPACKAGE_FIELDA");
        String fieldB = upgradeJsonObj.getString("UPPACKAGE_FIELDB");
        String fieldC = upgradeJsonObj.getString("UPPACKAGE_FIELDC");
        PrivateKey privteKeyObj = generatePrivateKey(d);
        String decrptFieldA = decryptRSA(privteKeyObj,fieldA);
        String decrptFieldB = decryptRSA(privteKeyObj,fieldB);
        String decrptFieldC = decryptAES(decrptFieldA,decrptFieldB,fieldC);
        JSONObject data = JSON.parseObject(decrptFieldC);

        //进行校验
        if(data.containsKey("licenseId") && nameObj.containsKey("o") && data.getString("licenseId").equals(nameObj.getString("o"))){
            return true;
        }
        return false;
    }

}
