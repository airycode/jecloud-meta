/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.login;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class    LoginSettingsSystemConfig extends SystemSetting {

    public LoginSettingsSystemConfig() {
        super("login-config", "登录设置");
        this.addItem(new ConfigItem("JE_CORE_VERIFY","登录方式", "", "","提示：用户可以通过用户名、账号（必选）、手机号、邮箱+密码的形式完成登录"));
        this.addItem(new ConfigItem("JE_CORE_PHONELOGIN","允许使用手机验证码登录", "","","提示：系统需提前正确配置手机短息服务。（平台配置-消息服务-手机配置）"));
        this.addItem(new ConfigItem("JE_CORE_PHONEREPASSWORD","允许使用手机找回密码","","提示1：系统需提前正确配置手机短息服务。（平台配置-消息服务-手机配置）\n" +
                "提示2：新密码设置需要遵循密码安全中的密码安全策略",""));
        this.addItem(new ConfigItem("JE_SYS_WXQUICKLOGIN","微信登录", "","","用户可绑定微信，用于快捷扫码登录"));
        this.addItem(new ConfigItem("JE_SYS_DDQUICKLOGIN","钉钉登录", "","","用户可绑定钉钉，用于快捷扫码登录"));
        this.addItem(new ConfigItem("JE_SYS_FSQUICKLOGIN","飞书登录", "","","用户可绑定飞书，用于快捷扫码登录"));
        this.addItem(new ConfigItem("JE_SYS_QQQUICKLOGIN","QQ登录", "","","用户可绑定  QQ，用于快捷扫码登录"));
        this.addItem(new ConfigItem("JE_SYS_WLQUICKLOGIN","WeLink登录", "","","用户可绑定 WeLink，用于快捷扫码登录"));
        this.addItem(new ConfigItem("JE_SYS_FASTLOGIN","快捷登录方式","","","WX,DD,FS,QQ,WL,APP"));
//        this.addItem(new ConfigItem("INITPASSWORD","密码校验策略", "","","提示：系统需要提前配置开放平台相关参数，且需要用户登录后自行绑定账号"));
//        this.addItem(new ConfigItem("WX_LOGIN_APPID","AppID","提示：应用ID（微信开放平台-->网站应用，申请地址：https://open.weixin.qq.com）",""));
//        this.addItem(new ConfigItem("WX_LOGIN_APPSECRET","AppSecret","提示：应用KEY(如果APP登录跟网站登录同步，则需要在开放平台将移动应用跟网站应用绑定)",""));
//        this.addItem(new ConfigItem("WX_LOGIN_APPURL","访问域名","提示：系统访问域名，格式为：https://www.baidu.com",""));

        this.addChild(new DingdingLoginSystemConfig());
        this.addChild(new FeishuLoginSystemConfig());
        this.addChild(new QqLoginSystemConfig());
        this.addChild(new WechatLoginSystemConfig());
        this.addChild(new WelinkLoginSystemConfig());
    }

}
