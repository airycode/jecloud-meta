/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.login;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 钉钉登录
 */
public class DingdingLoginSystemConfig extends SystemSetting {

    public DingdingLoginSystemConfig() {
        super("login-dingding", "钉钉登录");
        this.addItem(new ConfigItem("DD_LOGIN_CLIENTID","ClientId","","","提示：应用id，可使用扫码登录应用或者第三方个人小程序的appId，企业内部应用传应用的AppKey，第三方企业应用传应用的SuiteKey，第三方个人应用传应用的AppId，clientSecret：应用密钥"));
        this.addItem(new ConfigItem("DD_LOGIN_CLIENTSECRET","ClientSecret","","","提示：应用密钥，企业内部应用传应用的AppSecret，第三方企业应用传应用的SuiteSecret,第三方个人应传应用的AppSecret"));
        this.addItem(new ConfigItem("DD_LOGIN_REDIRECTURL","RedirectURL","","","提示：重定向地址"));

    }

}
