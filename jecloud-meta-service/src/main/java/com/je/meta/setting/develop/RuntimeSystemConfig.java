/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 平台配置-开发-运行
 */
public class RuntimeSystemConfig extends SystemSetting {

    public RuntimeSystemConfig() {
        super("runtime-config", "运行设置");
        this.addItem(new ConfigItem("JE_CORE_WEBSOCKETURL", "通讯链接服务地址（PC）", "", "","提示：websocket服务地址，配置错误会导致电脑应用无法正常使用（socket.io）"));
//        this.addItem(new ConfigItem("JE_CORE_WEBSOCKETURL4APP", "通讯链接服务地址（Mobile）", "", "","提示：开启以后系统会自动记录相关日志，请到功能中查看具体数据"));
        this.addItem(new ConfigItem("JE_SYS_LOGINFO", "日志开关", "", "",""));
        this.addItem(new ConfigItem("JE_SYS_INTERNET", "允许连接公网", "", "","提示：标记系统可以连接外网，当标记为否时部分请求将会主动放弃尝试连接"));
        this.addItem(new ConfigItem("JE_CORE_AJAXTIMEOUT", "Ajax全局请求时长", "", "","提示：设定前台Ajax请求的超时时长，默认为30秒"));
        this.addItem(new ConfigItem("JE_SYS_UPLOADFILESIZE", "允许上传附件全局大小", "", "","提示：全局设定附件组件允许上传的附件大小，默认10MB"));
        this.addItem(new ConfigItem("JE_DOCUMENT_FILE_SUFFIX", "允许上传附件格式", "", "",""));
    }

}
