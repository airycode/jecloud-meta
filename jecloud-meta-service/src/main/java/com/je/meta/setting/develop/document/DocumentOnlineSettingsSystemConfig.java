/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.document;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class DocumentOnlineSettingsSystemConfig extends SystemSetting {

    public DocumentOnlineSettingsSystemConfig() {
        super("document-online", "在线文档服务");
        this.addItem(new ConfigItem("JE_SYS_DOCUMENT", "文档选择开关", "", "","1onlyOffice，2WPS，3Not enabled"));
        this.addItem(new ConfigItem("JE_SYS_ONLYOFFICE","OnlyOffice","","","非国产开源软件，仅用预览功能免费，在线编辑功能收费，无需安装插件"));
        this.addItem(new ConfigItem("JE_SYS_WPS","WPS","","","国产收费，可以委托JECloud厂商代采，无需安装插件"));
        this.addItem(new ConfigItem("NOTENABLED","不启用","","","系统不提供office文件的在线编辑与浏览服务"));

        this.addChild(new OnlyOfficeDocumentSystemConfig());
        this.addChild(new WpsDocumentSystemConfig());
    }

}
