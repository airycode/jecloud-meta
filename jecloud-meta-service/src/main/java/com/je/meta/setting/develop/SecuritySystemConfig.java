/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class SecuritySystemConfig extends SystemSetting {

    public SecuritySystemConfig() {
        super("security-config", "安全设置");
        this.addItem(new ConfigItem("JE_SYS_ENCRYPT","浏览器数据传输加密","","","提示：开启后浏览器在关键数据传入时会使用密文传输"));
        this.addItem(new ConfigItem("JE_SYS_ENCRYPT_FIELD","传输时加密参数","","","提示：浏览器向后台传输数据时，对相关参数进行加密操作"));
        this.addItem(new ConfigItem("JE_SYS_ENCRYPT_KEY1","密钥1","","","提示：设定加密算法的密钥1"));
        this.addItem(new ConfigItem("JE_SYS_ENCRYPT_KEY2","密钥2","","","提示：设定加密算法的密钥2"));
        this.addItem(new ConfigItem("JE_SYS_ENCRYPT_KEY3","密钥3","","","提示：设定加密算法的密钥3"));
        this.addItem(new ConfigItem("JE_SMS_TIMEOUT","短信验证码时效","","","提示：短信睁眼吗的有效期，默认60秒"));
        this.addItem(new ConfigItem("JE_SYS_NOTE_COUNT","每日短信验证码生成次数","","","提示：每个手机号每日最多可以使用短信验证码次数，手机号不在用户表中则不予生成"));
        this.addItem(new ConfigItem("JE_SYS_STAYTIME","离开后保留登录状态时间","","","提示：停止操作后一段时间必须重新登录，默认168小时（7天），填写“-1“代表永不过期"));
        this.addItem(new ConfigItem("OPEN_ANTI_SQL_INJECTION","开启防SQL注入","","",""));
        this.addItem(new ConfigItem("OPEN_ANTI_XSS_ATTACK","开启防XSS攻击","","",""));

    }

}
