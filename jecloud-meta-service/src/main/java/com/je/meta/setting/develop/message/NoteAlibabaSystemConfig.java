/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.message;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 中国网建短信配置
 */
public class NoteAlibabaSystemConfig extends SystemSetting {

    public NoteAlibabaSystemConfig() {
        super("note-alicloud", "阿里云短信配置");
        this.addItem(new ConfigItem("NOTE_ALIYUN_SIGNNAME","SignName","","","提示：您可以登录短信服务控制台，选择国内消息或国际/港澳台消息，在签名管理页面获取"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_TEMPLATE","TemplateCode","","","提示：短息模板Code，您可以登录短信服务控制台，选择国内消息或国际/港澳台消息，在页面查看模板CODE，必须是已添加，并通过审核的短信模板，且发送国际/港澳台消息时，请使用模板。"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_ACCESSKEY","Accesskey ID","","","提示：用于标识用户"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_SECRET","AccessKey Secret","","","提示：用户验证用户的密钥，AccessKey Secret必须保密"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_URL","接入网址","","","选择自定义时可以设置服务地址"));
    }

}
