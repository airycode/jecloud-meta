/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class PasswordSystemConfig extends SystemSetting {

    public PasswordSystemConfig() {
        super("password-config", "密码安全");
        this.addItem(new ConfigItem("JE_SYS_PASSWORD","默认密码（password）","","","提示：系统新建用户时分配的默认密码"));
        this.addItem(new ConfigItem("INITPASSWORD","强制用户修改默认密码","","","提示：用户使用默认密码登陆系统后，将会被强行要求更换成新密码"));
        this.addItem(new ConfigItem("JE_CORE_ERRORPW","密码允许尝试次数","","","提示：密码输入超过指定次数以后用户会被锁定，需要管理员手动解锁"));
        this.addItem(new ConfigItem("REGEXP_NUMBER","密码安全策略-数字","","","提示：用户设置新密码时候必须要遵循的规则"));
        this.addItem(new ConfigItem("REGEXP","密码安全策略","","","提示：用户设置新密码时候必须要遵循的规则"));
        this.addItem(new ConfigItem("JE_REGEXP_ENABLE","是否开启密码安全策略","","","提示：密码输入超过指定次数以后用户会被锁定，需要管理员手动解锁"));
    }
}
