/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 平台配置-开发
 */
public class DevelopSystemConfig extends SystemSetting {

    public DevelopSystemConfig() {
        super("develop-online", "开发过程默认");
        this.addItem(new ConfigItem("FUNCINFO_CHILDSHOWTYPE", "【功能】子功能展示方式", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_COLUMN_WIDTH", "【列表】表格默认行高", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_PAGESIZE", "【列表】表格每页展示条数", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_PAGEINFOALIGN", "【列表】分页条展示位置", "","", ""));
        this.addItem(new ConfigItem("FUNCINFO_DISABLEQUERYSQL", "【列表】禁用查询元素", "","", ""));
        this.addItem(new ConfigItem("FUNCINFO_GRIDHIDELINES", "【列表】隐藏表格线", "","", ""));
        this.addItem(new ConfigItem("FUNCINFO_SIMPLEBAR", "【列表】启动简洁按钮条", "","", ""));
        this.addItem(new ConfigItem("FUNCINFO_FORMCOLS", "【表单】表单列数", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_FORMLABELWIDTH", "【表单】字段标题宽", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_FORMWIDTH", "【表单】表单宽度", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_FORMMINWIDTH", "【表单】表单最小宽度", "","", ""));
        this.addItem(new ConfigItem("FUNCINFO_FORM_POSITION", "【表单】弹出表单位置", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_FIELD_SPACING", "【表单】弹表字段间距", "", "",""));
        this.addItem(new ConfigItem("RESOURCEFIELD_BGCOLOR", "【表单】分组框背景颜色", "", "",""));
        this.addItem(new ConfigItem("RESOURCEFIELD_BORDER_COLOR", "【表单】分组框边框颜色", "", "",""));
        this.addItem(new ConfigItem("RESOURCEFIELD_TITLEB_COLOR", "【表单】分组框标题颜色", "", "",""));
        this.addItem(new ConfigItem("RESOURCEFIELD_TITLEB_BGCOLOR", "【表单】分组框标题背景颜色", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_TABLESTYLE", "【表单】启用科技灰皮肤", "", "",""));
        this.addItem(new ConfigItem("FUNCINFO_FORMFIELD_SPACING", "【表单】表单字段间距", "", "",""));
    }

}
