/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system.open;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class DingdingOpenPlatformSystemConfig extends SystemSetting {

    public DingdingOpenPlatformSystemConfig() {
        super("open-dingding-config", "钉钉开放配置");
        this.addItem(new ConfigItem("OPEN_DD_APPKEY","AppKey","","","提示：应用ID"));
        this.addItem(new ConfigItem("OPEN_DD_APPSECRET","AppSecret","","","提示：应用密钥"));
        this.addItem(new ConfigItem("OPEN_DD_CORPID","CorpId","","","提示：企业ID"));
        this.addItem(new ConfigItem("OPEN_DD_AGENTID","AgentID","","","提示：发送消息使用的微应用AgentID,企业内部应用可在开发者后台的应用详情页面查看，第三方企业应用可调用获取企业授权信息接口获取"));
    }

}
