/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.document;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class OnlyOfficeDocumentSystemConfig extends SystemSetting {

    public OnlyOfficeDocumentSystemConfig() {
        super("document-onlyoffice", "OnlyOffice配置");
        this.addItem(new ConfigItem("DOCUMENT_ONLY_HOST", "在线文档服务", "", "","提示：此地址是浏览器访问onlyoffice的地址"));
        this.addItem(new ConfigItem("DOCUMENT_ONLY_CALLHOST", "在线文档服务", "", "","提示：回调JEPaaS地址（onlyoffice可访问地址），此地址是onlyoffice访问JEPaaS系统的地址，用来下载文件及保存时回调JEPaaS"));
    }

}
