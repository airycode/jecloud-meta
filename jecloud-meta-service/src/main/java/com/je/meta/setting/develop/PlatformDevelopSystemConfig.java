/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop;

import com.je.meta.setting.SystemSetting;
import com.je.meta.setting.develop.document.DocumentOnlineSettingsSystemConfig;
import com.je.meta.setting.develop.document.DocumentSettingsSystemConfig;
import com.je.meta.setting.develop.login.LoginSettingsSystemConfig;
import com.je.meta.setting.system.TeamSystemConfig;

public class PlatformDevelopSystemConfig extends SystemSetting {

    public PlatformDevelopSystemConfig() {
        super("platform-develop", "平台配置-开发");
        this.addChild(new RuntimeSystemConfig());
        this.addChild(new SecuritySystemConfig());
        this.addChild(new NoteServiceSystemConfig());
        this.addChild(new LoginSettingsSystemConfig());
        this.addChild(new TeamSystemConfig());
        this.addChild(new DevelopSystemConfig());
        //在线文档 设置
        this.addChild(new DocumentOnlineSettingsSystemConfig());
        //文档设置
        this.addChild(new DocumentSettingsSystemConfig());
    }

}
