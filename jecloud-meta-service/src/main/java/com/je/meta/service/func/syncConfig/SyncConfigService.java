/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.syncConfig;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;

import java.util.List;

public interface SyncConfigService {

    //需要剔除的系统字段
    /*static final List<String> SYS_CODES = Lists.newArrayList("SY_STATUS", "SY_ORDERINDEX", "SY_CREATEORGID", "SY_CREATEORGNAME", "SY_CREATETIME",
            "SY_CREATEUSERID", "SY_CREATEUSERNAME", "SY_MODIFYORGID", "SY_MODIFYORGNAME", "SY_MODIFYUSERID","SY_TENANT_ID","SY_TENANT_NAME","SY_PARENT","SY_NODETYPE","SY_PARENTPATH","SY_LAYER",
            "SY_MODIFYUSERNAME", "SY_MODIFYTIME", "SY_MODIFYUSERNAME", "SY_COMPANY_ID", "SY_COMPANY_NAME", "SY_GROUP_COMPANY_ID", "SY_GROUP_COMPANY_NAME", "SY_ORG_ID","SY_PATH","SY_DISABLED","SY_TREEORDERINDEX","SY_ACKFLAG","SY_ACKUSERNAME",
            "SY_ACKUSERID","SY_ACKTIME","SY_AUDFLAG","SY_PIID","SY_PDID","SY_STARTEDUSER","SY_STARTEDUSERNAME","SY_APPROVEDUSERS","SY_APPROVEDUSERNAMES","SY_PREAPPROVUSERS","SY_PREAPPROVUSERNAMES","SY_LASTFLOWUSERID","SY_LASTFLOWUSER","SY_CURRENTTASK");
*/
    void selectResources(List<DynaBean> targetResourcesList, String way, String columnCodes);

    List<DynaBean> buildResources(List<DynaBean> targetResourcesList, List<DynaBean> currentResourcesList);

    void excuteSync(List<DynaBean> resources);
}
