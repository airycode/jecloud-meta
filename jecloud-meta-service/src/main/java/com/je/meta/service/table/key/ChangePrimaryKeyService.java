/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.key;

import com.je.common.base.DynaBean;

public interface ChangePrimaryKeyService {

    String checkLocal(DynaBean tableInfo);

    String checkMeta(String newPrimaryKey, DynaBean tableInfo);

    /**
     * 检查数据表是否有数据（注：分本地数据库与外部服务数据库）
     */
    String checkIsExistsData(String tableCode, String productCode);

    /**
     * 检查新输入的主键是否为当前表的其他字段
     * @param newPrimaryKey
     */
    boolean checkNewPrimaryKeyIsExistsOfCurrentTable(String tableCode, String newPrimaryKey);

    /**
     * 检查主键是否参与视图构建
     */
    String checkPrimaryKeyBelongToView(String pkValue, String pkCode);

    /**
     * 更新元数据
     * @param tableInfo
     * @param newPrimaryKey
     */
    void updateMeta(DynaBean tableInfo, String newPrimaryKey);
}
