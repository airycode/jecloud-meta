/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.ResFieldPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResFieldPlanServiceImpl implements ResFieldPlanService {

    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean doSave(DynaBean dynaBean) {
        String planId = JEUUID.uuid();
        dynaBean.setStr("JE_CORE_RESOURCEFIELD_PLAN_ID", planId);
        dynaBean.setStr("SY_STATUS", "1");
        dynaBean.setStr("PLAN_SY", "0");
        commonService.buildModelCreateInfo(dynaBean);
        String wheresql = " PLAN_FUNCINFO_ID = " + "'" + dynaBean.getStr("PLAN_PARENT_ID") + "'";
        commonService.generateOrderIndex(dynaBean, wheresql);
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCEFIELD_PLAN", ConditionsWrapper.builder().eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")));
        dynaBean.set("SY_ORDERINDEX", list.size() + 1);
        metaService.insert(dynaBean);
        String fieldStr = dynaBean.getStr("fieldStr");
        String fromPlanId = dynaBean.getStr("fromPlanId");
        if (StringUtil.isNotEmpty(fromPlanId)) {
            copyColumnConfig(fromPlanId, planId, fieldStr);
        }
        return dynaBean;
    }

    @Override
    public void doRemove(DynaBean dynaBean) {
        /**
         * 1.删除方案
         * 2.删除该方案的字段
         */
        metaService.delete(dynaBean.getTableCode(), ConditionsWrapper.builder()
                .eq("JE_CORE_RESOURCEFIELD_PLAN_ID", dynaBean.getStr("JE_CORE_RESOURCEFIELD_PLAN_ID")));

        metaService.delete("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder()
                .eq("RESOURCEFIELD_PLAN_ID", dynaBean.getStr("JE_CORE_RESOURCEFIELD_PLAN_ID")));
    }

    private void copyColumnConfig(String fromPlanId, String planId, String ids) {
        ConditionsWrapper wheresql = ConditionsWrapper.builder().eq("RESOURCEFIELD_PLAN_ID", fromPlanId);
        if (StringUtil.isNotEmpty(ids)) {
            wheresql.in("JE_CORE_RESOURCEFIELD_ID", Arrays.asList(ids.split(",")));
        }
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCEFIELD", wheresql);
        if (StringUtil.isEmpty(fromPlanId)) {
            throw new PlatformException(MessageUtils.getMessage("function.not.find"), PlatformExceptionEnum.JE_CORE_TABLE_UPDATE_ERROR);
        }
        for (DynaBean item : list) {
            DynaBean dynaBean = item;
            dynaBean.setStr("JE_CORE_RESOURCEFIELD_ID", JEUUID.uuid());
            dynaBean.setStr("RESOURCEFIELD_PLAN_ID", planId);
            commonService.buildModelCreateInfo(dynaBean);
            metaService.insert(dynaBean);
        }
    }
}
