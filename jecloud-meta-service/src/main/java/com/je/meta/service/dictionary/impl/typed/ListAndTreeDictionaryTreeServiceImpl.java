/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary.impl.typed;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.ConditionEnum;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.service.QueryBuilderService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.dd.DicInfoCache;
import com.je.meta.service.common.MetaBeanService;
import com.je.meta.service.dictionary.AbstractMetaDictionaryTreeServiceImpl;
import com.je.meta.service.dictionary.MetaDictionaryTreeService;
import com.je.meta.service.setting.MetaSystemSettingService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service("metaListAndTreeDictionaryTreeService")
public class ListAndTreeDictionaryTreeServiceImpl extends AbstractMetaDictionaryTreeServiceImpl implements MetaDictionaryTreeService {

    @Autowired
    private DicInfoCache dicInfoCache;
    @Autowired
    private MetaBeanService metaBeanService;
    @Autowired
    private QueryBuilderService queryBuilderService;
    @Autowired
    private MetaSystemSettingService systemSettingService;

    @Override
    public List<JSONTreeNode> loadAsynTree(JSONObject obj,String product, boolean en, boolean onlyItem) {
        //创建返回结果
        List<JSONTreeNode> array = new ArrayList<>();

        //根节点ID,默认为ROOT
        String rootId = obj.getString("rootId");
        if (StringUtil.isEmpty(rootId)) {
            rootId = ConstantVars.TREE_ROOT;
        }

        //字典编码
        String ddCode = obj.getString("ddCode");
        //字典名称，非必需字段
        String ddName = obj.getString("ddName");
        //字典标识，非必需字段 创建根节点对象ID使用,一次加载多个字典时使用
        String nodeInfo = obj.getString("nodeInfo");
        //默认为1  是否从根节点查找数据
        String isRoot = obj.containsKey("isRoot") ? obj.getString("isRoot") : "1";

        //构建根节点
        JSONTreeNode emptyRoot = new JSONTreeNode();
        emptyRoot.setText(ddName);
        emptyRoot.setParent("ROOT");
        emptyRoot.setId("ROOT_" + nodeInfo);
        emptyRoot.setNodeInfo(nodeInfo);

        //查找字典主表记录
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
        }
        if (dictionary == null) {
            array.add(emptyRoot);
            throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典类型，本方法支持[外部树形字典，列表字典，树形字典]
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        //字典对应表名，普通列表与普通树形字典此值为空
        String tableName = "JE_CORE_DICTIONARYITEM";

        if (!DDType.LIST.equalsIgnoreCase(ddType) && !DDType.TREE.equalsIgnoreCase(ddType)) {
            array.add(emptyRoot);
            throw new PlatformException("传入的字典类型出错，字典编码:【" + ddCode + "】!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //前端sql条件转换
        Query query;
        if (StringUtils.isNotBlank(obj.getString("j_query"))) {
            query = Query.build(obj.getString("j_query"));
        } else {
            query = new Query();
        }

        String adminPermSql = "";
        //条件构建器
        ConditionsWrapper wrapper = query.buildWrapper();
        //获取Admin权限sql
        String queryTableCode = obj.getString("queryTableCode");
        if (StringUtils.isNotBlank(queryTableCode)) {
            adminPermSql = getQuerySql(queryTableCode);
            if (StringUtils.isNotBlank(adminPermSql)) {
                final String sql = adminPermSql;
                wrapper.and(i -> i.apply(queryBuilderService.trimSql(sql)));
            }
        }

        // 普通字典
        wrapper.eq("DICTIONARYITEM_DICTIONARY_ID", dictionary.getStr("JE_CORE_DICTIONARY_ID"));
        //查找根节点
        if ("1".equals(isRoot)) {
            if (ConstantVars.TREE_ROOT.equals(rootId)) {
                DynaBean dynaBeanItem = metaService.selectOne("JE_CORE_DICTIONARYITEM",
                        ConditionsWrapper.builder().eq("DICTIONARYITEM_DICTIONARY_ID",dictionary.getStr("JE_CORE_DICTIONARY_ID"))
                                                    .eq("SY_NODETYPE","ROOT"));
                rootId = dynaBeanItem.getStr("JE_CORE_DICTIONARYITEM_ID");
            }
        }
        wrapper.eq("SY_FLAG", "1");

        //获取树形信息
        DynaBean table = metaBeanService.getResourceTable(tableName);
        if (table == null) {
            array.add(emptyRoot);
            return array;
        }

        //构建dyanBean的树形模版
        JSONTreeNode template = metaBeanService.buildJSONTreeNodeTemplate((List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS));
        //普通字典国际化处理
        if (en) {
            template.setText("DICTIONARYITEM_ITEMNAME_EN");
        }

        //字典配置sql条件
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL", "");

        // 添加字典配置sql
        if (StringUtil.isNotEmpty(ddWhereSql)) {
            //formatSqlParameter 替换Sql中的系统变量 非预处理赋值
            wrapper.apply(queryBuilderService.formatSqlParameter(ddWhereSql));
        }

        //排序条件
        String orderSql = query.buildOrder();
        if (StringUtils.isNotBlank(orderSql)) {
            orderSql = " ORDER BY " + orderSql;
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }
        wrapper.getParameter().put("orderSql", orderSql);

        //查询数据
        List<JSONTreeNode> jsonTreeNodeList = loadAsyncTreeNodeList(ddType,product,rootId, tableName, template, wrapper, "1".equals(isRoot), false);
        //添加根节点数据
        if ("1".equals(isRoot)) {
            JSONTreeNode root = new JSONTreeNode();
            root.setId(rootId);
            jsonTreeNodeList.add(root);
        }

        for (JSONTreeNode node : jsonTreeNodeList) {
            if (node.getId().equals(rootId) && "1".equals(isRoot)) {
                node.setText(ddName);
                node.setParent("ROOT");
                node.setDisabled("1");
                if (StringUtil.isNotEmpty(nodeInfo)) {
                    node.setNodeInfo(nodeInfo);
                    node.setId(node.getId() + "_" + nodeInfo);
                }
            } else {
                if (StringUtil.isNotEmpty(nodeInfo)) {
                    node.setNodeInfo(nodeInfo);
                    node.setId(node.getId() + "_" + nodeInfo);
                    node.setParent(node.getParent() + "_" + nodeInfo);
                }
            }
            node.setBean(new HashMap());
        }

        if (!"1".equals(isRoot)) {
            array.addAll(jsonTreeNodeList);
        }

        if (StringUtil.isNotEmpty(nodeInfo)) {
            rootId = rootId + "_" + nodeInfo;
        }
        JSONTreeNode rootNode = buildJSONNewTree(jsonTreeNodeList, rootId);

        if (onlyItem) {
            array.addAll(rootNode.getChildren());
        } else {
            array.add(rootNode);
        }

        return array;
    }

    @Override
    public List<JSONTreeNode> loadSyncTree(JSONObject obj,String product, boolean en, boolean onlyItem) {
        //构建返回结果
        List<JSONTreeNode> array = new ArrayList<>();

        //根节点ID,默认为ROOT
        String rootId = obj.getString("rootId");
        if (StringUtil.isEmpty(rootId)) {
            rootId = ConstantVars.TREE_ROOT;
        }

        //字典编码
        String ddCode = obj.getString("ddCode");
        //字典名称，非必需字段
        String ddName = obj.getString("ddName");
        //字典标识，非必需字段 创建根节点对象ID使用,一次加载多个字典时使用
        String nodeInfo = obj.getString("nodeInfo");
        //默认为1  是否从根节点查找数据
        String isRoot = obj.containsKey("isRoot") ? obj.getString("isRoot") : "1";

        //构建根节点
        JSONTreeNode emptyRoot = new JSONTreeNode();
        emptyRoot.setText(ddName);
        emptyRoot.setParent("ROOT");
        emptyRoot.setId("ROOT_" + nodeInfo);
        emptyRoot.setNodeInfo(nodeInfo);

        //查找字典项
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
            if (dictionary == null) {
                throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
            }
        }

        //字典类型
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        if(!DDType.TREE.equals(ddType) && !DDType.LIST.equals(ddType)){
            throw new PlatformException("元数据模块不支持此字典类型：" + ddType + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典对应表名或类名
        String tableName = "JE_CORE_DICTIONARYITEM";

        //字典配置sql
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL", "");

        Query query;
        if (obj.containsKey("j_query")) {
            query = Query.buildWithObject(obj.getJSONObject("j_query"));
        } else {
            query = new Query();
        }

        List<Condition> customQuery = (List<Condition>)obj.get("customQuery");
        query.addCustoms(JSONArray.toJSONString(customQuery));

        //排序条件
        String orderSql = "";
        if (!query.getOrder().isEmpty()) {
            orderSql = " ORDER BY " + query.buildOrder();
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }

        //获取资源表信息
        DynaBean table = metaBeanService.getResourceTable(tableName);
        if (table == null) {
            array.add(emptyRoot);
            return array;
        }

        //构建树形模板
        JSONTreeNode template = metaBeanService.buildJSONTreeNodeTemplate((List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS));
        //使用英文
        if (en) {
            template.setText("DICTIONARYITEM_ITEMNAME_EN");
        }

        // 增加条件字典ID
        query.addCustom("DICTIONARYITEM_DICTIONARY_ID", ConditionEnum.EQ, dictionary.getStr("JE_CORE_DICTIONARY_ID"));

        // 查找根节点
        if ("1".equals(isRoot)) {
            if (ConstantVars.TREE_ROOT.equals(rootId)) {
                DynaBean dynaBeanItem = metaService.selectOne("JE_CORE_DICTIONARYITEM",
                        ConditionsWrapper.builder().eq("DICTIONARYITEM_DICTIONARY_ID",dictionary.getStr("JE_CORE_DICTIONARY_ID"))
                                .eq("SY_NODETYPE","ROOT"));
                rootId = dynaBeanItem.getStr("JE_CORE_DICTIONARYITEM_ID");
            }
        }
        query.addCustom("SY_FLAG", ConditionEnum.EQ, "1");

        ConditionsWrapper selectDic = query.buildWrapper();
        // 添加字典配置sql
        if (StringUtil.isNotEmpty(ddWhereSql)) {
            //formatSqlParameter 替换Sql中的系统变量 非预处理赋值
            selectDic.apply(queryBuilderService.formatSqlParameter(ddWhereSql));
        }

        //排序条件
        selectDic.getParameter().put("orderSql", orderSql);
        List<JSONTreeNode> jsonTreeNodeList = loadTreeNodeList(ddType,product,rootId, tableName, template, selectDic);
        for (JSONTreeNode node : jsonTreeNodeList) {
            if (node.getId().equals(rootId)) {
                node.setText(ddName);
                node.setParent("ROOT");
                node.setDisabled("1");
                if (StringUtil.isNotEmpty(nodeInfo)) {
                    node.setNodeInfo(nodeInfo);
                    node.setId(node.getId() + "_" + nodeInfo);
                }
            } else {
                if (StringUtil.isNotEmpty(nodeInfo)) {
                    node.setNodeInfo(nodeInfo);
                    node.setId(node.getId() + "_" + nodeInfo);
                    node.setParent(node.getParent() + "_" + nodeInfo);
                }
            }
            if ("JE_CORE_DICTIONARYITEM".equals(tableName)) {
                node.setBean(new HashMap<>());
            }
        }

        if (StringUtil.isNotEmpty(nodeInfo)) {
            rootId = rootId + "_" + nodeInfo;
        }

        JSONTreeNode rootNode = buildJSONNewTree(jsonTreeNodeList, rootId);
        rootNode.setText(ddName);
        rootNode.setParent("ROOT");

        if (onlyItem) {
            array.addAll(rootNode.getChildren());
        } else {
            array.add(rootNode);
        }

        return array;
    }

    @Override
    public List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean en, Query query) {
        if (StringUtil.isEmpty(rootId)) {
            rootId = ConstantVars.TREE_ROOT;
        }

        List<JSONTreeNode> array = new ArrayList<>();
        //查询字典主表信息
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
        }
        if (dictionary == null) {
            throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR);
        }

        //字典配置项
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL", "");
        String tableName = dictionary.getStr("DICTIONARY_CLASSNAME");

        //前端sql条件转换
        //排序条件
        String orderSql = "";
        if (!query.getOrder().isEmpty()) {
            orderSql = " ORDER BY " + query.buildOrder();
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }

        if (DDType.LIST.equalsIgnoreCase(ddType) || DDType.TREE.equalsIgnoreCase(ddType)) {
            //手动添加字典：列表字典 树形字典
            // 是否自维护字典
            tableName = "JE_CORE_DICTIONARYITEM";

            //获取资源表信息
            DynaBean table = metaBeanService.getResourceTable(tableName);
            if (table == null) {
                return array;
            }

            // 增加条件字典ID
            query.addCustom("DICTIONARYITEM_DICTIONARY_ID", ConditionEnum.EQ, dictionary.getStr("JE_CORE_DICTIONARY_ID"));

            // 普通字典
            // 查找根节点
            if (ConstantVars.TREE_ROOT.equals(rootId)) {
                DynaBean dynaBeanItem = metaService.selectOne("JE_CORE_DICTIONARYITEM",
                        ConditionsWrapper.builder().eq("DICTIONARYITEM_DICTIONARY_ID",dictionary.getStr("JE_CORE_DICTIONARY_ID"))
                                .eq("SY_NODETYPE","ROOT"));
                rootId = dynaBeanItem.getStr("JE_CORE_DICTIONARYITEM_ID");
            }
            query.addCustom("SY_FLAG", ConditionEnum.EQ, "1");
            orderSql = " ORDER BY SY_ORDERINDEX";

        }

        ConditionsWrapper selectDic = query.buildWrapper();
        // 添加字典配置sql
        if (StringUtil.isNotEmpty(ddWhereSql)) {
            //formatSqlParameter 替换Sql中的系统变量 非预处理赋值
            selectDic.apply(queryBuilderService.formatSqlParameter(ddWhereSql));
        }

        //获取树形信息
        DynaBean table = metaBeanService.getResourceTable(tableName);
        if (table == null) {
            return array;
        }

        //构建树形模板
        JSONTreeNode template = metaBeanService.buildJSONTreeNodeTemplate((List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS));
        //使用英文
        if (en) {
            template.setText("DICTIONARYITEM_ITEMNAME_EN");
        }

        String idField = template.getId();
        String codeField = template.getCode();
        String textField = template.getText();
        String parentField = template.getParent();

        //父级数据条件
        if (StringUtil.isNotEmpty(parentId)) {
            selectDic.eq(parentField, parentId);
        } else if (StringUtil.isNotEmpty(parentCode)) {
            // 为了支持客户允许地区可以多选 yangzhi改于20191111
            String[] split = parentCode.split(ArrayUtils.SPLIT);
            String parentCodes = StringUtil.buildArrayToString(split);
            List<DynaBean> parents = metaService.select(tableName, selectDic.clone().apply(" AND " + codeField + " in(" + parentCodes + ")"));
            if (!parents.isEmpty()) {
                ArrayList<String> ids = Lists.newArrayList();
                for (DynaBean bean : parents) {
                    ids.add(bean.getStr(idField, ""));
                }
                //父节点条件
                selectDic.in(parentField, ids);
//                        querySql += " AND " + parentField + " in(" + StringUtil.buildArrayToString(list) + ")";
            } else {
                throw new PlatformException("数据错误，根据CODE【" + parentCode + "】未找到父项值!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWNPARENT_ERROR);
            }
        } else {
            selectDic.eq(parentField, rootId);
        }

        //排序条件
        selectDic.apply(orderSql);

        //查询数据
        List<DynaBean> datas = metaService.select(tableName, selectDic);
        for (DynaBean data : datas) {
            JSONTreeNode node = new JSONTreeNode();
            if (StringUtil.isNotEmpty(idField)) {
                node.setId(data.getStr(idField));
            }
            if (StringUtil.isNotEmpty(codeField)) {
                node.setCode(data.getStr(codeField));
            }
            if (StringUtil.isNotEmpty(textField)) {
                node.setText(data.getStr(textField));
            }
            if (StringUtil.isNotEmpty(parentField)) {
                node.setParent(data.getStr(parentField));
            }
            node.setBean(data.getValues());
            array.add(node);
        }

        return array;
    }
}
