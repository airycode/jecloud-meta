/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.converter;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.table.ColumnType;
import com.je.meta.model.database.ColumnDescription;

public class DatabaseMysqlConverter extends AbstractIDateBaseConverter {

    @Override
    public void buildTableColumnTypeAndLength(DynaBean dynaBean, ColumnDescription columnDescription) {
        String type = columnDescription.getFieldTypeName();
        int precisionSize = columnDescription.getScaleSize();
        int length = columnDescription.getPrecisionSize();
        if (type.startsWith(ColumnType.VARCHAR)) {
            dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR);
            dynaBean.set("TABLECOLUMN_LENGTH", "");
            if (255 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR255);
            } else if (100 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR100);
            } else if (50 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR50);
            } else if (30 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR30);
            } else if (767 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR767);
            } else if (1000 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR1000);
            } else if (2000 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR2000);
            } else if (4000 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.VARCHAR4000);
            } else if (4 == length) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.YESORNO);
            } else if (length == 536870911) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.BIGCLOB);
            } else if (length == 16383) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.CLOB);
            } else if (length > 4000) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.CLOB);
            } else {
                dynaBean.set("TABLECOLUMN_LENGTH", length);
            }
        } else if (type.startsWith("INT") || type.startsWith("BIGINT")) {
            dynaBean.set("TABLECOLUMN_TYPE", ColumnType.NUMBER);
        } else if (type.startsWith(ColumnType.FLOAT) || type.startsWith("NUMERIC")
                || type.startsWith("DECIMAL") || type.startsWith(ColumnType.NUMBER)) {
            if (precisionSize <= 0) {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.NUMBER);
                if (length == 20) {
                    dynaBean.set("TABLECOLUMN_LENGTH", "");
                } else {
                    dynaBean.set("TABLECOLUMN_LENGTH", length);
                }
            } else {
                dynaBean.set("TABLECOLUMN_TYPE", ColumnType.FLOAT);
                if (length == 20) {
                    dynaBean.set("TABLECOLUMN_LENGTH", precisionSize);
                } else {
                    dynaBean.set("TABLECOLUMN_LENGTH", length + "," + precisionSize);
                }
            }
        } else if (type.startsWith("TIMESTAMP")) {
            dynaBean.set("TABLECOLUMN_TYPE", ColumnType.DATETIME);
        } else if (type.startsWith("TEXT")) {
            dynaBean.set("TABLECOLUMN_TYPE", "CLOB");
        } else if (type.startsWith("LONGTEXT")) {
            dynaBean.set("TABLECOLUMN_TYPE", "BIGCLOB");
        } else {
            dynaBean.set("TABLECOLUMN_TYPE", ColumnType.CUSTOM);
            dynaBean.set("TABLECOLUMN_LENGTH", type);
        }
    }
}
