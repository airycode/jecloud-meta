/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.permission;

/**
 * 权限模板,模板使用spring expression解析器
 */
public enum PermissionFuncDataTemplateEnum {

    //--------------------------PC功能数据权限（本人数据操作）模板---------------------------
    FUNC_PC_DATA_SELF_SHOW("本人看本人","  (${#createUserIdField} = '${#userId}')","本人查看本人数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_SELF_UPDATE("本人更改本人","","本人更新本人数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_SELF_DELETE("本人删除本人","","本人删除本人数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（部门主管数据操作）模板---------------------------
    FUNC_PC_DATA_DEPTLEADER_SHOW("部门领导可见","  ${#createDeptIdField} = '${#deptId}'","部门主管查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_DEPTLEADER_UPDATE("部门领导可更新","","部门主管更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_DEPTLEADER_DELETE("部门领导可删除","","部门主管删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（部门直属领导数据操作）模板---------------------------
    FUNC_PC_DATA_DIRECTLEADER_SHOW("直接领导可见"," (${#createUserIdField} in (${#userIdList}))","部门直接领导查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_DIRECTLEADER_UPDATE("直接领导可更新","","部门直接领导更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_DIRECTLEADER_DELETE("直接领导可删除","","部门直接领导删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（监管领导数据操作）模板---------------------------
    FUNC_PC_DATA_MONITORLEADER_SHOW("监管领导可见","  ${#createDeptIdField} IN (${#monitorDeptIdList})","监管领导查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_MONITORLEADER_UPDATE("监管领导可更新","","监管领导更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_MONITORLEADER_DELETE("监管领导可删除","","监管领导删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（本部门数据操作）模板---------------------------
    FUNC_PC_DATA_MYDEPT_SHOW("本部门可见","  ${#createDeptIdField} = '${#deptId}'","本部门人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_MYDEPT_UPDATE("本部门可更新","","本部门人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_MYDEPT_DELETE("本部门可删除","","本部门人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（工作流待审批数据操作）模板---------------------------
    FUNC_PC_DATA_MYAPPROVE_PENDING_SHOW("待审批可见", "  SY_PREAPPROVUSERS LIKE '%${#pendingUserId}%'", "工作流待审批人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_MYAPPROVE_PENDING_UPDATE("待审批可更新","","工作流待审批人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_MYAPPROVE_PENDING_DELETE("待审批可删除","","工作流待审批人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（工作流待审批后数据操作）模板---------------------------
    FUNC_PC_DATA_MYAPPROVE_PENDED_SHOW("审批后可见","  SY_APPROVEDUSERS LIKE '%${#pendedUserId}%'","工作流审批后人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_MYAPPROVE_PENDED_UPDATE("审批后可更新","","工作流审批后人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_MYAPPROVE_PENDED_DELETE("审批后可删除","","工作流审批后人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（公司数据操作）模板---------------------------
    FUNC_PC_DATA_COMPANY_SHOW("本公司内可见", "  ${#companyIdFieldCode} = '${#companyId}'", "公司人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_COMPANY_UPDATE("本公司可更新","","公司人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_COMPANY_DELETE("本公司可删除","","公司人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（集团公司数据操作）模板---------------------------
    FUNC_PC_DATA_GROUPCOMPANY_SHOW("集团公司内可见", "  ${#groupCompanyIdFieldCode} = '${#groupCompanyId}'","集团公司人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_GROUPCOMPANY_UPDATE("集团公司内可更新","","集团公司人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_GROUPCOMPANY_DELETE("集团公司内可删除","","集团公司人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（公司监管数据操作）模板---------------------------
    FUNC_PC_DATA_MONITORCOMPANY_SHOW("监管公司内可见","  ${#companyIdFieldCode} IN (${#monitorCompanyIdList})","公司监管人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_MONITORCOMPANY_UPDATE("监管公司内可更新","","公司监管人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_MONITORCOMPANY_DELETE("监管公司内可删除","","公司监管人员删除数据权限，此权限为鉴权类型权限"),

    //--------------------------PC功能数据权限（部门监管部门数据操作）模板---------------------------
    FUNC_PC_DATA_DEPTMONITORDEPT_SHOW("部门监管部门可见", "  ${#createDeptIdField} IN (${#monitorDeptList})", "公司监管人员查看数据权限，此权限为输出权限，输出SQL"),
    FUNC_PC_DATA_DEPTMONITORDEPT_UPDATE("部门监管部门可更新","","公司监管人员更新数据权限，此权限为鉴权类型权限"),
    FUNC_PC_DATA_DEPTMONITORDEPT_DELETE("部门监管部门可删除","","公司监管人员删除数据权限，此权限为鉴权类型权限"),

    FUNC_PC_DATA_SEEUSERS_SHOW("可见人员","  ${#createUserIdField} IN (${#seeUserIds})","可见人员"),
    FUNC_PC_DATA_SEEUSERS_UPDATE("可见人员更新","","可见人员更新"),
    FUNC_PC_DATA_SEEUSERS_DELETE("可见人员删除","","可见人员删除"),
    FUNC_PC_DATA_SEEDEPTS_SHOW("可见部门","  ${#createDeptIdField} IN (${#seeDeptIds})","可见部门"),
    FUNC_PC_DATA_SEEDEPTS_UPDATE("可见部门更新","","可见部门更新"),
    FUNC_PC_DATA_SEEDEPTS_DELETE("可见部门删除","","可见部门删除"),
    FUNC_PC_DATA_SEEROLES_SHOW("可见角色","  ${#createUserIdField} IN (${#seeUserIds})","可见角色"),
    FUNC_PC_DATA_SEEROLES_UPDATE("可见角色更新","","可见角色更新"),
    FUNC_PC_DATA_SEEROLES_DELETE("可见角色删除","","可见角色删除"),
    FUNC_PC_DATA_SEEORGS_SHOW("可见机构","  SY_ORG_ID IN (${#seeOrgIds})","可见机构"),
    FUNC_PC_DATA_SEEORGS_UPDATE("可见机构更新","","可见机构更新"),
    FUNC_PC_DATA_SEEORGS_DELETE("可见机构删除","","可见机构删除"),
    ;

    private String name;
    private String outputTemplate;
    private String desc;

    public String getName() {
        return name;
    }

    public String getOutputTemplate() {
        return outputTemplate;
    }

    public String getDesc() {
        return desc;
    }

    PermissionFuncDataTemplateEnum(String name, String outputTemplate, String desc) {
        this.name = name;
        this.outputTemplate = outputTemplate;
        this.desc = desc;
    }

}
