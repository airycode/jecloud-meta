/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.entity;

public class ButtonEntity {
    /**
     * 按钮名称
     */
    private String text;
    /**
     * 图标
     */
    private String icon;
    /**
     * js脚本
     */
    private String js;
    /**
     * 显隐表达式
     */
    private String expression;

    /**
     * 类型
     */
    private String type;
    /**
     * 启用
     */
    private String enable;

    /**
     * 依附按钮code
     */
    private String relyCode;
    /**
     * 依附按钮name
     */
    private String relyName;
    /**
     * 是否是系统字段
     */
    private String systemField;

    /**
     * code
     */
    private String code;
    /**
     *
     * @param text
     * @param icon
     * @param js
     * @param expression
     * @param type
     * @param enable
     */

    public ButtonEntity(String text,String code, String icon, String js, String expression, String type, String enable, String relyCode,String relyName,String systemField) {
        this.text = text;
        this.code =code;
        this.icon = icon;
        this.js = js;
        this.expression = expression;
        this.type = type;
        this.enable = enable;
        this.relyCode =relyCode;
        this.relyName =relyName;
        this.systemField = systemField;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getJs() {
        return js;
    }

    public void setJs(String js) {
        this.js = js;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }


    public String getSystemField() {
        return systemField;
    }

    public void setSystemField(String systemField) {
        this.systemField = systemField;
    }

    public String getRelyCode() {
        return relyCode;
    }

    public void setRelyCode(String relyCode) {
        this.relyCode = relyCode;
    }

    public String getRelyName() {
        return relyName;
    }

    public void setRelyName(String relyName) {
        this.relyName = relyName;
    }

    @Override
    public String toString() {
        return "ButtonEntity{" +
                "text='" + text + '\'' +
                ", icon='" + icon + '\'' +
                ", js='" + js + '\'' +
                ", expression='" + expression + '\'' +
                ", type='" + type + '\'' +
                ", enable='" + enable + '\'' +
                ", relyCode='" + relyCode + '\'' +
                ", relyName='" + relyName + '\'' +
                ", systemField='" + systemField + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
