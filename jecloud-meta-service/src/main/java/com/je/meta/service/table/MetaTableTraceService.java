/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table;

import com.je.common.base.DynaBean;

/**
 * 资源表元数据日志服务定义
 */
public interface MetaTableTraceService {

    /**
     * 构建表数据的数据留痕
     *
     * @param tableCode 操作的表编码
     * @param oldBean   原实体
     * @param newBean   新实体
     * @param oper      操作
     * @param tableId   表主键
     * @param isCreate  是否应用
     */
    void saveTableTrace(String tableCode, DynaBean oldBean, DynaBean newBean, String oper, String tableId, String isCreate);

    void saveTableTrace(String tableCode, DynaBean dynaBean, String oper, String tableId, String isCreate);

}
