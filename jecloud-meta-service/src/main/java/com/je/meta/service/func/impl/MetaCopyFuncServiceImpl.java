/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.MetaCopyFuncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MetaCopyFuncServiceImpl implements MetaCopyFuncService {

    @Autowired
    private MetaService metaService;

    @Override
    public void copyColumn(String newFuncId, String oldFuncId) {

        List<DynaBean> list = metaService.select("JE_CORE_RESOURCECOLUMN",
                ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID",oldFuncId));

        for(DynaBean dynaBean:list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_RESOURCECOLUMN");
            dynaBean.set("JE_CORE_RESOURCECOLUMN_ID", null);
            dynaBean.set("RESOURCECOLUMN_NEWFUNCID", null);
            dynaBean.set("RESOURCECOLUMN_FUNCINFO_ID", newFuncId);
            metaService.insert(dynaBean);
        }
    }

    @Override
    public void copyField(String newFuncId, String oldFuncId) {

        List<DynaBean> list = metaService.select("JE_CORE_RESOURCEFIELD",
                ConditionsWrapper.builder().eq("RESOURCEFIELD_FUNCINFO_ID",oldFuncId));


        for(DynaBean dynaBean:list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_RESOURCEFIELD");
            dynaBean.set("JE_CORE_RESOURCEFIELD_ID", null);
            dynaBean.set("RESOURCEFIELD_NEWFUNCID", null);
            dynaBean.set("RESOURCEFIELD_FUNCINFO_ID", newFuncId);
            metaService.insert(dynaBean);
        }

    }

    @Override
    public void copyDataFlow(String newFuncId, String oldFuncId) {
        List<DynaBean> list = metaService.select("JE_CORE_DATAFLOW",
                ConditionsWrapper.builder().eq("DATAFLOW_FUNCINFO_ID",oldFuncId));


        for(DynaBean dynaBean:list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_DATAFLOW");
            dynaBean.set("JE_CORE_DATAFLOW_ID", null);
            dynaBean.set("DATAFLOW_FUNCINFO_ID", newFuncId);
            metaService.insert(dynaBean);
        }
    }

    @Override
    public void copyButton(String newFuncId, String oldFuncId) {
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCEBUTTON",
                ConditionsWrapper.builder().eq("RESOURCEBUTTON_FUNCINFO_ID", oldFuncId));


        for(DynaBean dynaBean:list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_RESOURCEBUTTON");
            dynaBean.set("RESOURCEBUTTON_FUNCINFO_ID", newFuncId);
            dynaBean.set("JE_CORE_RESOURCEBUTTON_ID", null);
            dynaBean.set("RESOURCEBUTTON_NEWFUNCID", null);
            metaService.insert(dynaBean);
        }
    }

    @Override
    public void copyQuerys(String newFuncId, String oldFuncId) {
        List<DynaBean> list = metaService.select("JE_CORE_GROUPQUERY",
                ConditionsWrapper.builder().eq("GROUPQUERY_GNID", oldFuncId));

        for(DynaBean dynaBean :list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_GROUPQUERY");
            //去除主键
            dynaBean.remove("JE_CORE_GROUPQUERY_ID");
            //使用新功能ID
            dynaBean.set("GROUPQUERY_GNID", newFuncId);
            metaService.insert(dynaBean);
        }
    }

    @Override
    public void copyStrategies(String newFuncId,String funcCode ,String oldFuncId) {
        List<DynaBean> list = metaService.select("JE_CORE_QUERYSTRATEGY",
                ConditionsWrapper.builder().eq("QUERYSTRATEGY_FUNCINFO_ID",oldFuncId));


        for(DynaBean dynaBean: list){
            dynaBean.set(BeanService.KEY_TABLE_CODE, "JE_CORE_QUERYSTRATEGY");
            //去除主键
            dynaBean.remove("JE_CORE_QUERYSTRATEGY_ID");
            //使用新功能编码
            dynaBean.set("QUERYSTRATEGY_FUNCCODE", funcCode);
            dynaBean.setStr("QUERYSTRATEGY_FUNCINFO_ID", newFuncId);
            metaService.insert(dynaBean);
        }
    }
}
