/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.framework.product.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.PlatformService;
import com.je.meta.service.framework.product.ProductService;
import com.je.rbac.rpc.RoleRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService, CommonRequestService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private PlatformService manager;
    @Autowired
    private RoleRpcService roleRpcService;
    @Autowired
    private CommonService commonService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public DynaBean doSave(BaseMethodArgument param, HttpServletRequest request) {
        String developerIds = getStringParameter(request, "PRODUCT_DEVELOPER_ID");
        String productId = getStringParameter(request, "JE_PRODUCT_MANAGE_ID");

        DynaBean productRole = roleRpcService.findProductDevelopRole(productId);
        if (productRole != null) {
            roleRpcService.bindUserToProductRole(productRole.getStr("JE_RBAC_ROLE_ID"), Strings.isNullOrEmpty(developerIds) ? new ArrayList<>() : Splitter.on(",").splitToList(developerIds));
        }
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        commonService.generateOrderIndex(dynaBean,"");
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);
        return dynaBean;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public DynaBean doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        String developerIds = getStringParameter(request, "PRODUCT_DEVELOPER_ID");
        String productId = getStringParameter(request, "JE_PRODUCT_MANAGE_ID");

        DynaBean productRole = roleRpcService.findProductDevelopRole(productId);
        if (productRole != null) {
            List<String> nowDevelopIdList;
            if (Strings.isNullOrEmpty(developerIds)) {
                nowDevelopIdList = new ArrayList<>();
            } else {
                nowDevelopIdList = Splitter.on(",").splitToList(developerIds);
            }
            roleRpcService.bindUserToProductRole(productRole.getStr("JE_RBAC_ROLE_ID"), nowDevelopIdList);
        }

        return manager.doUpdate(param, request);
    }

}
