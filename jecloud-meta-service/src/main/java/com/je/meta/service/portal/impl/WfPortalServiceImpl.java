/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.portal.impl;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.portal.service.WfPortalService;
import com.je.common.base.service.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WfPortalServiceImpl implements WfPortalService {

    @Autowired
    private MetaService metaService;

    @Override
    public JSONObject getWfNum(String userId) {
        JSONObject numObj = new JSONObject();
        numObj.put("delayCount", 0);
        return numObj;
    }

    @Override
    public JSONObject getWfBadge(String userId) {
        JSONObject numObj = new JSONObject();
        //过滤条件
        StringBuffer whereSql = new StringBuffer();
        //排除审阅和会签的子任务节点
        whereSql.append(" AND EXECUTION_  IS NOT NULL AND SUPERTASK_ IS NULL ");
        //加入执行流程过滤
        String enabledSql = "AND EXECUTION_ID_ IN (SELECT ID_ FROM JBPM4_EXECUTION WHERE PROCDEFID_ in ( SELECT PROCESSINFO_PROCESSDEFINIT_ID FROM JE_CORE_PROCESSINFO  WHERE PROCESSINFO_DEPLOYSTATUS='1' AND PROCESSINFO_ENABLED='1'))";
        whereSql.append(enabledSql);
        String handUpSql = " AND EXECUTION_ID_ NOT IN (SELECT ID_ FROM JBPM4_HIST_PROCINST WHERE STATE_='HANDUPED')";
        whereSql.append(handUpSql);
        String querySql = "";
        Long count = new Long(0);
        whereSql.append(handUpSql);
        // AND GROUPID_='countersign'
        String preapprovSql = whereSql.toString() + " AND DELAYTASK_='0' AND (ASSIGNEE_ like '%" + userId + "%' OR  DBID_ IN (SELECT TASK_ FROM JBPM4_PARTICIPATION WHERE USERID_='" + userId + "' AND TYPE_='viewer' AND HANDLE_='0')) AND DBID_ NOT IN (SELECT TASK_ FROM JBPM4_PARTICIPATION WHERE USERID_='" + userId + "' AND TYPE_='client' AND HANDLE_='1')";
        //待审批-流程工作台
        Long preapprovCount = metaService.countBySql("SELECT COUNT(*) FROM JBPM4_TASK WHERE 1=1 " + preapprovSql);
        numObj.put("badge", preapprovCount);
        return numObj;
    }

}
