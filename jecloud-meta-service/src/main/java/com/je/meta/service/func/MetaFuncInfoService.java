/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func;

import com.je.common.base.DynaBean;
import com.je.common.base.entity.func.FuncInfo;
import com.je.common.base.entity.func.FuncRelationField;
import com.je.core.entity.extjs.JSONTreeNode;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface MetaFuncInfoService {

    /**
     * 导入按钮
     *
     * @param funInfo TODO未处理
     * @param funId   TODO未处理
     * @author sunwanxiang
     * @date 2012-3-16 下午01:17:35
     */
    void implButton(DynaBean funInfo, String funId);

    /**
     * 获取json的功能对象
     *
     * @param funcCode 功能编码
     * @return
     */
    FuncInfo getFuncInfo(String funcCode);

    /**
     * 获取功能配置对象
     *
     * @param funcInfo 功能信息
     * @return
     */
    Map<String, Object> getFuncConfigInfo(DynaBean funcInfo);

    /**
     * 构建功能默认信息
     *
     * @param funcInfo TODO未处理
     */
    void buildDefaultFuncInfo(DynaBean funcInfo);

    /**
     * 更新功能信息(包括对按钮导入、软连接、资源表等做处理)
     *
     * @param funcInfo TODO未处理
     */
    DynaBean updateFunInfo(DynaBean funcInfo);

    /**
     * 删除功能   (包括对功能权限 按钮权限清除。 级联更新父节点信息 、删除挂有该功能的子功能信息和软连接关系清除)
     *
     * @param funcId TODO未处理
     */
    void removeFuncInfoById(String funcId);

    /**
     * 软连接复制
     *
     * @return
     */
    //DynaBean copySoftFuncInfo(DynaBean newFunInfo, DynaBean oldFunInfo);

    /**
     * 构建主子功能关联字段的查询条件
     *
     * @param relatedFields TODO未处理
     * @param dynaBean      TODO未处理
     */
    String buildWhereSql4funcRelation(List<FuncRelationField> relatedFields, DynaBean dynaBean);

    /**
     * 清空功能
     *
     * @param funcInfo TODO未处理
     */
    DynaBean clearFuncInfo(DynaBean funcInfo);

    /**
     * 初始化功能
     *
     * @param funcInfo TODO未处理
     */
    void initFuncInfo(DynaBean funcInfo);

    /**
     * 解除软连接关系
     *
     * @param funcRelyonId TODO未处理
     */
    void removeFuncRelyon(String funcRelyonId);

    /**
     * 功能拖动
     *
     * @param dynaBean
     */
    DynaBean treeMove(DynaBean dynaBean);

    /**
     * 功能字段配置同步
     *
     */
    void syncConfig(DynaBean dynaBean);

    /**
     * 清空功能的缓存
     *
     * @param funcCodes 功能编码
     */
    void clearFuncAllStatize(String funcCodes);

    String translate(DynaBean dynaBean);

    /**
     * 保存功能
     */
    DynaBean doSave(DynaBean dynaBean);

    List<Map<String, Object>> getJsEvent(DynaBean dynaBean);

    /**
     * 获取功能字典
     * @param funcIds
     * @return
     */
    List<DynaBean> findFuncInfoDics(String funcIds);

    Map<String, Object> getInformationById(String pkValue,String code,String type);

    Map<String, String> getWorkflowByfuncCode(String code);

    void doUpdateFuncCode(DynaBean dynaBean);

    /**
     * 检查功能是否开启工作流，对应表中有没有流程相关字段，没有的话则新建
     * @param tableName
     * @param productCode
     * @param resourceTableId
     */
    void checkFuncUseWf(String tableName, String productCode, String resourceTableId);

    DynaBean copyFunc(String newFuncName, String newFuncCode, String parentId, DynaBean oldFuncInfo);

    JSONTreeNode getFileInfoList(String funcRelationId,String dataId,String funcCode);
}
