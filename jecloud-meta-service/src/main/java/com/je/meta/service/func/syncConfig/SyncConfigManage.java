/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.syncConfig;

import com.je.common.base.DynaBean;

import java.util.List;

public class SyncConfigManage {

    private List<DynaBean> targetResourcesList;

    private List<DynaBean> currentResourcesList;

    //同步资源 列表和表单
    private SyncConfigService syncConfigService;
    //1：全部同步；2：选中字段同步；3：排除选中字段
    private String syncWay;

    private String columnCodes;

    public SyncConfigManage(List<DynaBean> targetResourcesList, List<DynaBean> currentResourcesList,
                            SyncConfigService syncConfigService, String syncWay, String columnCodes){
        this.targetResourcesList = targetResourcesList;
        this.currentResourcesList = currentResourcesList;
        this.syncConfigService =syncConfigService;
        this.syncWay = syncWay;
        this.columnCodes= columnCodes;
    }

    public void excuteSync(){
        /**
         * 1.剔除不要的字段
         * 2.构建配置信息
         * 3.同步
         */
        syncConfigService.selectResources(targetResourcesList,syncWay,columnCodes);

        List<DynaBean> resourcesList = syncConfigService.buildResources(targetResourcesList,currentResourcesList);

        syncConfigService.excuteSync(resourcesList);
    }
}
