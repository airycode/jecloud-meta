/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.common;

public class DevLogType {
    /**
     * 开发类型
     */
    public enum DevLogTypeEnum {
        FUNC("功能"),
        DIC("字典"),
        TABLE("资源表"),
        MENU("菜单"),
        CHART("图表"),
        REPORT("报表"),
        WF("工作流"),
        SYSTEM("系统");

        private String name;

        DevLogTypeEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    /**
     * 动作类型
     */
    public enum DevLogActionEnum {
        CREATE("创建"),
        UPDATE("修改"),
        DELETE("删除"),
        APPLY("应用"),
        VERSION("发布");
        private String name;

        DevLogActionEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }
}
