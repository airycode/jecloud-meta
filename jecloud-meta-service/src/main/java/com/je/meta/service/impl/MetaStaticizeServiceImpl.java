/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.CacheManageRpcService;
import com.je.common.base.service.rpc.UpgradeModulePackageRpcService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.func.FuncInfoCache;
import com.je.meta.cache.func.FuncStaticizeCache;
import com.je.meta.cache.table.DynaCache;
import com.je.meta.cache.table.TableCache;
import com.je.meta.rpc.func.MetaFuncPermService;
import com.je.meta.service.MetaStaticizeService;
import com.je.servicecomb.RpcSchemaFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.je.servicecomb.JECloud.*;

@Service
public class MetaStaticizeServiceImpl implements MetaStaticizeService {

    @Autowired
    private FuncInfoCache funcInfoCache;
    @Autowired
    private FuncStaticizeCache funcStaticizeCache;

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaFuncPermService metaFuncPermService;

    @Autowired
    private DynaCache dynaCache;
    @Autowired
    private TableCache tableCache;

    @Override
    @Transactional
    public void doFuncInfo(String funcCode, String funcValue, String funcEnValue) {
        funcStaticizeCache.putCache(funcCode, funcValue);
        if(StringUtil.isNotEmpty(funcEnValue)){
            funcStaticizeCache.putCache(funcCode+"_en", funcEnValue);
        }
    }

    @Override
    @Transactional
    public void clearFuncAllStatize(String funcCodes, Boolean allZh) {
        if(StringUtil.isNotEmpty(funcCodes)){
            for(String funcCode:funcCodes.split(",")){
                funcInfoCache.removeCache(funcCode);
                funcStaticizeCache.removeCache(funcCode);
            }
        }
    }

    @Override
    public void removeCache(String funcCode,String product) {
        String currentTenantId = SecurityUserHolder.getCurrentAccountTenantId();
        if(StringUtil.isNotEmpty(funcCode)){
            DynaBean function =metaService.selectOne("JE_CORE_FUNCINFO",ConditionsWrapper.builder().eq("FUNCINFO_FUNCCODE",funcCode));
            if(function==null){
                return;
            }
            String tableCode = function.getStr("FUNCINFO_TABLENAME");
            if(StringUtil.isNotEmpty(currentTenantId)){
                //功能缓存
                funcInfoCache.removeCache(currentTenantId,funcCode);
                funcStaticizeCache.removeCache(currentTenantId,funcCode);
                tableCache.removeCache(currentTenantId,tableCode);
                dynaCache.removeCache(currentTenantId,tableCode);

            }else{
                //功能缓存
                funcInfoCache.removeCache(funcCode);
                funcStaticizeCache.removeCache(funcCode);
                metaFuncPermService.restoreDefault(funcCode);
                //资源表缓存
                tableCache.removeCache(tableCode);
                dynaCache.removeCache(tableCode);
            }
            String productCode = function.getStr("SY_PRODUCT_CODE");
            if (PRODUCT_CORE_META.equals(productCode)) {
                metaService.clearMyBatisTableCache(tableCode);
                metaService.clearMyBatisFuncCache(funcCode);
            }else {
                CacheManageRpcService cacheManageRpcService = RpcSchemaFactory.getRemoteProvierClazz(productCode,
                        "cacheManageRpcService", CacheManageRpcService.class);
                cacheManageRpcService.removeIbatisCache(funcCode,tableCode);
            }
        }else {
            funcInfoCache.clear();
            funcStaticizeCache.clear();
            if (PRODUCT_CORE_META.equals(product)) {
                metaService.clearMyBatisFuncCache();
                metaService.clearMyBatisCache();
            }else {
                CacheManageRpcService cacheManageRpcService = RpcSchemaFactory.getRemoteProvierClazz(product,
                        "cacheManageRpcService", CacheManageRpcService.class);
                cacheManageRpcService.removeIbatisCache(null,null);
            }

        }


    }
}
