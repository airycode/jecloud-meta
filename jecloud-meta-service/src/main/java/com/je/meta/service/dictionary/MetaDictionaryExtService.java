/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary;

import com.je.ibatis.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

/**
 * 数据字典拓展功能
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/7/31
 */
public interface MetaDictionaryExtService {

    /**
     * 字典信息插入日志表
     *
     * @param code 字典编码
     */
    void addLog(String code);

    /**
     * 查找未被使用过的字典（僵尸字典）
     *
     * @return 字典信息
     */
    List<Map<String, Object>> selectNotUseDictionary();

    /**
     * 查找被删除的字典（丢失字典）
     *
     * @return 字典信息
     */
    List<Map<String, Object>> selectNotFindDictionary(String sql, Page page);

    /**
     * 生成字典摘要信息
     *
     * @param idArr 字典主键
     */
    String generateDictionaryResumeToMeta(String[] idArr);

    /**
     * 查询被表单字段使用的字典
     * @return
     */
    List<Map<String, Object>> selectFormFieldUsedList();
    List<Map<String, Object>> selectFormFieldUsedListBySql(String sql);
}
