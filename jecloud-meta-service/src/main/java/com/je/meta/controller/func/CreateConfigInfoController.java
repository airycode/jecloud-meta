/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/je/meta/createConfig")
public class CreateConfigInfoController extends AbstractPlatformController {

    private static final String _NAME = "_NAME";
    private static final String _CODE = "_CODE";
    private static final String _ID = "_ID";
    @Autowired
    private MetaService metaService;

    /**
     * 生成配置信息
     *
     * @param param
     */
    @RequestMapping(value = "/dicConfigInfo", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult createDicConfigInfo(BaseMethodArgument param, HttpServletRequest request) {
        String funcId = getStringParameter(request, "funcId");
        String fieldCode = getStringParameter(request, "fieldCode");
        if (Strings.isNullOrEmpty(funcId) || Strings.isNullOrEmpty(fieldCode)) {
            return BaseRespResult.errorResult("参数不可以为空！");
        }
        //fieldCode 处理
        String resultFieldCode;
        if (fieldCode.endsWith(_CODE)) {
            resultFieldCode = fieldCode.substring(0, fieldCode.length() - 5);

        } else if (fieldCode.endsWith(_NAME)) {
            resultFieldCode = fieldCode.substring(0, fieldCode.length() - 5);

        } else if (fieldCode.endsWith(_ID)) {
            resultFieldCode = fieldCode.substring(0, fieldCode.length() - 5);

        } else {
            resultFieldCode = fieldCode;
        }
        List<DynaBean> dynaBeanList = metaService.select("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID", funcId)
                .likeRight("RESOURCECOLUMN_CODE", resultFieldCode), "RESOURCECOLUMN_CODE");

        StringBuilder configInfoStr = new StringBuilder();
        if (dynaBeanList != null && dynaBeanList.size() > 1) {
            List<String> columnList = new ArrayList<>();
            for (DynaBean dynaBean : dynaBeanList) {
                columnList.add(dynaBean.getStr("RESOURCECOLUMN_CODE"));
            }

            if (columnList.contains(resultFieldCode + _ID) && columnList.contains(resultFieldCode + _NAME) && columnList.contains(resultFieldCode + _CODE)) {
                configInfoStr.append(resultFieldCode).append(_CODE);
                configInfoStr.append("~");
                configInfoStr.append(resultFieldCode).append(_NAME);
                configInfoStr.append("~");
                configInfoStr.append(resultFieldCode).append(_ID);
                configInfoStr.append(",code~text~id");
            } else if (!columnList.contains(resultFieldCode + _ID) && columnList.contains(resultFieldCode + _NAME) && columnList.contains(resultFieldCode + _CODE)) {
                configInfoStr.append(resultFieldCode).append(_CODE);
                configInfoStr.append("~");
                configInfoStr.append(resultFieldCode).append(_NAME);
                configInfoStr.append(",code~text");
            } else if (columnList.contains(resultFieldCode + _ID) && !columnList.contains(resultFieldCode + _NAME) && columnList.contains(resultFieldCode + _CODE)) {
                configInfoStr.append(resultFieldCode).append(_CODE);
                configInfoStr.append("~");
                configInfoStr.append(resultFieldCode).append(_ID);
                configInfoStr.append(",code~id");
            } else if (columnList.contains(resultFieldCode + _ID) && columnList.contains(resultFieldCode + _NAME) && !columnList.contains(resultFieldCode + _CODE)) {
                configInfoStr.append(resultFieldCode).append(_NAME);
                configInfoStr.append("~");
                configInfoStr.append(resultFieldCode).append(_ID);
                configInfoStr.append(",text~id");
            } else {
                configInfoStr.append(fieldCode);
                configInfoStr.append(",code");
            }
            return BaseRespResult.successResult(configInfoStr);
        } else {
            configInfoStr.append(fieldCode);
            configInfoStr.append(",code");
            return BaseRespResult.successResult(configInfoStr);
        }
    }
}
