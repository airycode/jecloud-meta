/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.variable;

import com.alibaba.fastjson2.JSON;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.cache.variable.FrontCache;
import com.je.meta.service.variable.MetaSysVariablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/meta/variable")
public class SystemVariableController extends AbstractPlatformController {

    @Autowired
    private FrontCache frontCache;
    @Autowired
    private MetaSysVariablesService metaSysVariablesService;

    @Override
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        if (param.getLimit() == 0) {
            param.setLimit(-1);
        }

        //分页对象
        Page page = new Page<>(param.getPage(), param.getLimit());
        String funcCode = param.getFuncCode();
        //构建查询条件
        Query query = param.buildQuery();
        commonService.buildProductQuery(query);
        param.setjQuery(JSON.toJSONString(query));
        ConditionsWrapper wrapper = manager.buildWrapper(param, request);
        List<Map<String, Object>> list = metaService.load(funcCode, page, wrapper);
        return BaseRespResult.successResultPage(list,Long.valueOf(page.getTotal()));
    }

    /**
     * 获取前端系统变量(前端用) 前端加载主页会调用
     */
    @RequestMapping(value = "/loadSysVariables", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadSysVariables(BaseMethodArgument param, HttpServletRequest request) {
        //系统变量
        Map<String, String> frontVar = frontCache.getCacheValues();
        if (null == frontVar) {
            metaSysVariablesService.reloadSystemVariables();
            frontVar = frontCache.getCacheValues();
        }
        Map<String, Object> results = new HashMap<>();
        if (frontVar != null) {
            results.putAll(frontVar);
        }
        return BaseRespResult.successResult(results);
    }


}
