/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.func.MetaFuncConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/je/develop/funcConfig")
public class FuncConfigController extends AbstractPlatformController {

    @Autowired
    private MetaFuncConfigService configService;

    @Override
    @RequestMapping(
            value = {"/doSave"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean config = (DynaBean) request.getAttribute("dynaBean");
        if (config == null || StringUtils.isAnyBlank(config.getStr("JE_CORE_FUNCINFO_ID"), config.getStr("CONFIG_FUNCCODE"))) {
            throw new PlatformException("参数不完整！", PlatformExceptionEnum.JE_FUNCINFO_ERROR);
        }
        configService.save(config);
        return BaseRespResult.successResult(config.getValues());
    }

    @Override
    @RequestMapping(
            value = {"/doUpdate"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return doSave(param, request);
    }

    @Override
    @RequestMapping(
            value = {"/load"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        //获取参数
        String funcId = getStringParameter(request, "funcId");
        String funcCode = getStringParameter(request, "funcCode");
        String type = getStringParameter(request, "type");
        String status = getStringParameter(request, "status");
        //查询配置项
        List<DynaBean> list = configService.load(funcId, funcCode, type, status);
        //返回数据
        return BaseRespResult.successResultPage(list, (long) list.size());
    }

}
