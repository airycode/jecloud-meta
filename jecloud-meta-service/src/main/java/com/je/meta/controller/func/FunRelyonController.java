/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.FunCopyType;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 * 子体母体关联关系
 */
@RestController
@RequestMapping(value = "/je/develop/funRelyon")
public class FunRelyonController extends AbstractPlatformController {

    /**
     * 解除关联关系
     *
     * @param param
     */
    @RequestMapping(value = "/removeRelyon", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult removeRelyon(BaseMethodArgument param) {
        String pkValue = param.getPkValue();
        StringBuffer ids = new StringBuffer();
        DynaBean fr = metaService.selectOneByPk("JE_CORE_FUNCRELYON", pkValue);
        if (fr == null) {
            return BaseRespResult.errorResult("构建失败!");
        }
        //得到功能
        ids.append(fr.getStr("JE_CORE_FUNCRELYON_ID"));
        //母体功能
        DynaBean funInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", fr.getStr("FUNCRELYON_FUNCINFO_ID"), "JE_CORE_FUNCINFO_ID,FUNCINFO_FUNCCODE");
        String funCode = fr.getStr("FUNCRELYON_FUNCODE");
        //子体功能
        DynaBean ztFunc = metaService.selectOne("JE_CORE_FUNCINFO", ConditionsWrapper.builder()
                .apply(" FUNCINFO_FUNCCODE={0}", funCode), "JE_CORE_FUNCINFO_ID");
        List<DynaBean> rels = metaService.select(ConditionsWrapper.builder().table("JE_CORE_FUNCRELYON")
                .apply("FUNCRELYON_FUNCINFO_ID={0}", ztFunc.getStr("JE_CORE_FUNCINFO_ID")));
        for (DynaBean re : rels) {
            if (funInfo.getStr("FUNCINFO_FUNCCODE").equals(re.getStr("FUNCRELYON_FUNCCODE")) && FunCopyType.MT.equals(re.getStr("FUNCRELYON_REGARD"))) {
                ids.append(",");
                ids.append(re.getStr("JE_CORE_FUNCRELYON_ID"));
            }
        }
        metaService.delete(ConditionsWrapper.builder().table("JE_CORE_FUNCRELYON").in("JE_CORE_FUNCRELYON_ID", Arrays.asList(ids.toString().split(","))));
        return BaseRespResult.successResult(null, "解除关系成功!");
    }

}
